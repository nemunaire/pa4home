<?php
//Définition de la constante anti-hacking
define("INDEX", 1);

ob_start();

//Inclusion de l'API Onyx
require_once(trim(file_get_contents('./.onyx')));
require_once(ONYX.'include/functions.php');

$SESS = new Session();

//Création du fichier XML
$xml = new DOMDocument('1.0', 'UTF-8');
$xml->formatOutput = true;
$xml_root = $xml->createElement("root");
$xml->appendChild($xml_root);

//sleep(1);

$demande = gpc('d');
$value = intval(gpc('i'));

if ($demande == "connecte")
{
    $nom = strtolower(gpc("name", "post"));
    $mdp = hash("sha512", $nom.'♂♫↨'.gpc("mdp", "post"));

    $bdd = new BDD();
    $bdd->escape($nom);
    $bdd->query("UPDATE users SET last_ip = '".$_SERVER["REMOTE_ADDR"]."', last_visite = ".time()." WHERE pseudo = '$nom' AND password = '$mdp';");
    $affected = $bdd->affected();
    if ($affected)
      $user = $bdd->unique_query("SELECT * FROM users WHERE pseudo = '$nom' AND password = '$mdp';");
    $bdd->deconnexion();

    if ($affected != 0)
    {
	$SESS->values["connecte"] = true;
	$SESS->values["username"] = $nom;
	$SESS->values["id_user"] = $user["id"];
	$SESS->put($user["id"]);
    }
    else
    {
	$SESS->values["connecte"] = false;
	$SESS->values["username"] = "";
	$SESS->values["id_user"] = 0;
	$SESS->put();
    }
}
elseif ($demande == "logout")
{
    $SESS->values["connecte"] = false;
    $SESS->values["username"] = "";
    $SESS->close();
}
elseif ($demande == "accueil")
{
    $dir = opendir(APPSDIR);
    while (($app = readdir($dir)) !== false)
    {
	if (is_dir(APPSDIR.$app) && is_file(APPSDIR.$app.'/property.xml'))
	{
	    $applicationXML = new DOMDocument();
	    $applicationXML->load(APPSDIR.$app.'/property.xml');

	    if ($applicationXML->documentElement->getAttribute('active') && $applicationXML->getElementsByTagName('application'))
	    {
		//Vérification de sécurités
		if (!acces_application($app, $applicationXML))
		  continue;

		$xml_appli = $xml->createElement("application");
		$xml_appli->appendChild($xml->createElement("dir", $app));
		foreach($applicationXML->getElementsByTagName('property') as $property)
		{
		    $xml_appli->appendChild($xml->createElement($property->getAttribute('value'), $property->textContent));
		}
		$xml_root->appendChild($xml_appli);
	    }
	}
    }
    closedir($dir);
}
elseif ($demande == "property" && $app = gpc('a'))
{
    if (empty($app) || ereg('/', $app) || !is_file(APPSDIR.$app.'/property.xml'))
      send404($xml_root);
    else
    {
	$appXML = new DOMDocument();
	$appXML->load(APPSDIR.$app.'/property.xml');

	if ($appXML->documentElement->getAttribute('active') && $appXML->getElementsByTagName('application'))
	{
	    //Vérification de sécurités
	    if (!acces_application($app, $appXML))
	      continue;

	    $xml_appli = $xml->createElement("property");
	    foreach($appXML->getElementsByTagName('property') as $property)
	    {
		$xml_appli->appendChild($xml->createElement($property->getAttribute('value'), $property->textContent));
	    }
	    $xml_appli->appendChild($xml->createElement("dir", $app));
	    $xml_root->appendChild($xml_appli);
	}
    }
}
elseif ($demande == "display" && $app = gpc('a'))
{
    if (empty($app) || ereg('/', $app) || !is_file(APPSDIR.$app.'/property.xml'))
      send404($xml_root);
    else
    {
	$appXML = new DOMDocument();
	$appXML->load(APPSDIR.$app.'/property.xml');

	if ($appXML->documentElement->getAttribute('active') && $appXML->getElementsByTagName('application'))
	{
	    //Vérification de sécurités
	    if (!acces_application($app, $appXML))
	      continue;

	    require(APPSDIR.$app.'/main.php');

	    $xml_root->appendChild($xml->createElement("dir", $app));

	    $xml_appli = $xml->createElement("display");
	    foreach($appXML->getElementsByTagName('display') as $contenu)
	    {
		if ($contenu->getAttribute('value') == "css")
		{
		    $xml_css = $xml->createElement($contenu->getAttribute('value'), $contenu->textContent);
		    if ($contenu->getAttribute('media'))
		      $xml_css->setAttribute("media", $contenu->getAttribute('media'));
		    $xml_appli->appendChild($xml_css);
		}
		else
		  $xml_appli->appendChild($xml->createElement($contenu->getAttribute('value'), $contenu->textContent));
	    }
	    $xml_root->appendChild($xml_appli);

	    $xml_appli = $xml->createElement("property");
	    foreach($appXML->getElementsByTagName('property') as $property)
	    {
		$xml_appli->appendChild($xml->createElement($property->getAttribute('value'), $property->textContent));
	    }

	    if ($appXML->getElementsByTagName('menu'))
	    {
		$menu = array();
		foreach($appXML->getElementsByTagName('menu') as $item)
		  $menu[] = array("text" => $item->getAttribute('text'), "eventClick" => $item->getAttribute('eventClick'));
				
		$xml_appli->appendChild($xml->createElement("menu", json_encode($menu)));
	    }
	    $xml_root->appendChild($xml_appli);
	}
    }
}
elseif ($demande == "action" && $app = gpc('a'))
{
    if (empty($app) || ereg('/', $app) || !is_file(APPSDIR.$app.'/property.xml'))
      send404($xml_root);
    else
    {
	$appXML = new DOMDocument();
	$appXML->load(APPSDIR.$app.'/property.xml');

	if ($appXML->documentElement->getAttribute('active') && $appXML->getElementsByTagName('application'))
	  require(APPSDIR.$app.'/main.php');
    }
}
elseif ($demande == "page" && $page = gpc('p'))
{
    if (empty($page) || ereg('/', $page) || !is_file(PAGESDIR.$page.'.xml'))
      send404($xml_root);
    else
    {
	$pageXML = new DOMDocument();
	$pageXML->load(PAGESDIR.$page.'.xml');

	if ($pageXML->documentElement->getAttribute('active') && $pageXML->getElementsByTagName('page'))
	{
	    //Vérification de sécurité
	    if (!acces_application($page, $pageXML))
	      send403($xml_root);
	    else
	    {
		foreach($pageXML->getElementsByTagName('display') as $contenu)
		{
		    $xml_root->appendChild($xml->createElement($contenu->getAttribute('value'), $contenu->textContent));
		}
	    }
	}
    }
}

$output = ob_get_contents();
ob_end_clean();

if (!empty($output))
  $xml_root->appendChild($xml->createElement("output", $output));

$xml_root->setAttribute("ping", time());
$xml_root->setAttribute("statut", intval(!empty($SESS->values["connecte"])));
if (!empty($SESS->values["connecte"]))
  $xml_root->setAttribute("username", $SESS->values["username"]);

print $xml->saveXML();
