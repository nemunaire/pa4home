var GSPC_origin_liste = false;
var GSPC_liste = false;
var GSPC_lastSort = -1;
var GSPC_delay = null;
var nbParPage = 50;

//On charge la liste des porte-clés
function GSPC_loadList()
{
    GSPC_origin_liste = false;
    GSPC_liste = false;
    GSPC_lastSort = -1;
    $('#nbpc').html('<img src="applications/GSPC/loader.gif" alt="Chargement en cours ...">');

    $.ajax({
	url: "ajax.php",
	data: {d: "action", a: "GSPC", p: "liste"},
    }).done(GSPC_parseList).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}
GSPC_loadList();

//Une fois la liste reçue, on la parse
function GSPC_parseList(data)
{
    $('#nbpc').html(data.documentElement.getAttribute("nombre"));

    var liste = data.documentElement.getElementsByTagName("liste")[0].getElementsByTagName("porteclef");
    GSPC_origin_liste = new Array();

    for (var i = 0; i < liste.length; i++)
    {
	var elt = liste[i];
	GSPC_origin_liste.push([elt.getAttribute("id"), elt.getAttribute("nom"), elt.getAttribute("caracteristique"), elt.getAttribute("ligne"), elt.getAttribute("special")]);
    }
}

//Met en place le formulaire d'ajout de porte-clé
function GSPC_add()
{
    if (GSPC_delay)
	clearTimeout(GSPC_delay);

    $('#contenu').html("<h2>Ajout d'un porte-clef à la base de données</h2>");

    GSPC_addScreen(false);
}

//Affiche la liste des porte-clés
function GSPC_list()
{
    if (GSPC_delay)
	clearTimeout(GSPC_delay);

    window.scrollTo(0,0);

    $('#contenu').html("<h2>Liste des porte-clefs</h2>");

    GSPC_liste = false;
    GSPC_lastSort = -1;

    GSPC_viewliste();
}

//Affiche la page de statistiques
function GSPC_stats()
{
    if (GSPC_delay)
	clearTimeout(GSPC_delay);

    window.scrollTo(0,0);

    $('#contenu').html("<h2>Statistiques</h2>");

    var tableau = document.createElement("div");
    tableau.className = "table cols2";
    tableau.id = "stats";
    var tableau_head = document.createElement("div");
    tableau_head.className = "head";
    var head_th =  document.createElement("span");
    head_th.innerHTML = "Ligne";
    tableau_head.appendChild(head_th);
    var head_th =  document.createElement("span");
    head_th.innerHTML = "Nombre";
    tableau_head.appendChild(head_th);
    tableau.appendChild(tableau_head);
    var tableau_body = document.createElement("div");
    tableau_body.className = "body";
    tableau_body.innerHTML = '<div class="lign elt0 foot">Téléchargement des statistiques en cours ... <img src="applications/GSPC/loader.gif" alt="Veuillez patienter"></div>';
    tableau.appendChild(tableau_body);
    $('#contenu').append(tableau);

    $.ajax({
	url: "ajax.php",
	data: { d: "action", a: "GSPC", p: "stats" }
    }).done(function(data) {
	var stats = data.documentElement.getElementsByTagName("statistiques")[0];
	var nbPC = stats.getElementsByTagName("ligne").length;

	tableau_body.innerHTML = "";

	for (var i = 0; i < nbPC; i++)
	{
	    if (stats.getElementsByTagName("ligne")[i].getAttribute("nom") != "")
	    {
		var lign = document.createElement("div");
		lign.className = "elt" + (i%2);
		GSPC_addStatLign(lign, stats.getElementsByTagName("ligne")[i].getAttribute("nom"));
		var col = document.createElement("span");
		col.innerHTML = stats.getElementsByTagName("ligne")[i].getAttribute("nom");
		lign.appendChild(col);
		var col = document.createElement("span");
		col.innerHTML = stats.getElementsByTagName("ligne")[i].getAttribute("nombre");
		lign.appendChild(col);
		tableau_body.appendChild(lign);
	    }
	}
	lign.className += " foot";
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

function GSPC_addStatLign(lign, ligne)
{
    lign.onclick = function() { window.scrollTo(0,0); GSPC_viewligne(ligne) };
}

function GSPC_viewligne(ligne)
{
    $('#contenu').html("<h2>Liste des porte-clefs de la ligne " + ligne + "</h2>");

    GSPC_delay_viewligne(ligne);
}

function GSPC_delay_viewligne(ligne)
{
    if (GSPC_origin_liste == false)
	GSPC_delay = setTimeout(GSPC_delay_viewligne, 200, ligne);
    else
    {
	GSPC_liste = new Array();
	for (var i = 0; i < GSPC_origin_liste.length; i++)
	{
	    if (GSPC_origin_liste[i][3] == ligne)
		GSPC_liste.push(GSPC_origin_liste[i]);
	}
	GSPC_lastSort = -1;

	GSPC_viewliste();
    }
}

function GSPC_addScreen(modif)
{
    formulaire = document.createElement("form");
    formulaire.id = "add";
    formulaire.className = "form-horizontal";
    formulaire.onsubmit = function() { checkAndAdd(modif); return false; }
    formulaire.innerHTML = '<div class="form-group"><label for="nompc" class="col-sm-5 control-label">Nom/marque :</label><div class="col-sm-7"><input type="text" id="nompc" maxlength="250" class="form-control"></div></div>    <div class="form-group"><label for="caracpc" class="col-sm-5 control-label">Caractéristiques :</label><div class="col-sm-7"><input type="text" id="caracpc" maxlength="250" class="form-control"></div></div>    <div class="form-group"><label for="lignepc" class="col-sm-5 control-label">Ligne :</label><div class="col-sm-7"><input type="text" id="lignepc" maxlength="3" size="5" class="form-control"></div></div>    <div class="form-group"><label for="quantitepc" class="col-sm-5 control-label">Quantité :</label><div class="col-sm-7"><select class="form-control" id="quantitepc"><option value="1">Simple</option><option value="2">Double</option><option value="3">Triple</option><option value="4">Quadruple</option></select></div></div>';

    if(modif)
    {
	formulaire.innerHTML += '<button type="submit" class="btn btn-primary">Modifier</button>';

	inp_del = document.createElement("button");
	inp_del.type = "button";
	inp_del.className = "btn btn-danger";
	inp_del.innerHTML = '<span class="glyphicon glyphicon-trash"></span> Supprimer';
	inp_del.style.marginLeft = "15px";
	formulaire.appendChild(inp_del);
    }
    else
	formulaire.innerHTML += '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Ajouter</button>';

    $('#contenu').append(formulaire);

    if(modif)
    {
	$('#nompc').val(GSPC_liste[modif-1][1]);
	$('#caracpc').val(GSPC_liste[modif-1][2]);
	$('#lignepc').val(GSPC_liste[modif-1][3]);
	$('#quantitepc').val(GSPC_liste[modif-1][4]);
    }

    $('#nompc').focus();
    $('#add button.btn-danger').click(function() { GSPC_del(modif); });

    confirmation = document.createElement("h3");
    confirmation.style.color = "teal";
    confirmation.id = "confirm";
    $('#contenu').append(confirmation);
}

function GSPC_del(modif)
{
    $('#nompc').prop("disabled", true);
    $('#caracpc').prop("disabled", true);
    $('#lignepc').prop("disabled", true);
    $('#quantitepc').prop("disabled", true);

    var ligne = GSPC_liste[modif-1][3];

    $.ajax({
	url: "ajax.php?d=action&a=GSPC&p=del",
	type: "post",
	data: { id: GSPC_liste[modif-1][0] },
    }).done(function(data) {
	if (data.documentElement.getAttribute("statut") != 1)
	{
	    firstLoad();
	    alert("Vous avez été déconnecté. Le porte-clef n'a pas été supprimé");
	}
	else
	{
	    $('#contenu').html('<div class="alert alert-success"><strong>Porte-clef supprimé avec succès</strong></div>');
	    $('#nbpc').innerHTML = data.documentElement.getAttribute("nombre");

	    GSPC_loadList();
	    GSPC_delay_viewligne(ligne);
	}
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });

    $('#contenu').innerHTML = "<h2 class=\"vp\">Veuillez patienter suppression du porte-clef en cours ...</h2>";
}

function checkAndAdd(modif)
{
    if(modif)
	modif = "&id=" + GSPC_liste[modif-1][0];
    else
	modif = "";

    $('#nompc').parent().parent().removeClass("has-error");
    $('#lignepc').parent().parent().removeClass("has-error");
    if ($('#nompc').val() == "")
    {
	$('#nompc').parent().parent().addClass("has-error");
	alert('Veuillez indiquer une marque ou un nom !');
	$('#nompc').focus();
    }
    else if ($('#lignepc').val() == "")
    {
	$('#lignepc').parent().parent().addClass("has-error");
	alert('Veuillez indiquer une ligne !');
	$('#lignepc').focus();
    }
    else
    {
	$('#nompc').prop("disabled", true);
	$('#caracpc').prop("disabled", true);
	$('#lignepc').prop("disabled", true);
	$('#quantitepc').prop("disabled", true);
	
	$.ajax({
	    url: 'ajax.php?d=action&a=GSPC' + modif,
	    type: "POST",
	    data: { nom: escapeHtml($('#nompc').val()),
		    caracteristique: escapeHtml($('#caracpc').val()),
		    ligne: escapeHtml($('#lignepc').val()),
		    quantite: escapeHtml($('#quantitepc').val())
		  },
	}).done(function(data) {
	    if (data.documentElement.getAttribute("statut") != 1)
	    {
		firstLoad();
		alert("Vous avez été déconnecté. Le porte-clef n'a pas été enregistré");
	    }
	    else
	    {
		GSPC_loadList();

		if(modif)
		{
		    $('#contenu').html('<div class="alert alert-success">Porte-clef modifié avec succès !</div>');
		    $('#nbpc').html(data.documentElement.getAttribute("nombre"));

		    GSPC_delay_viewligne(lign);
		}
		else
		{
		    $('#nompc').val("");
		    $('#nompc').parent().parent().removeClass("has-error");
		    $('#caracpc').val("");
		    $('#lignepc').parent().parent().removeClass("has-error");
		    $('#quantitepc').val(1);

		    $('#nompc').prop("disabled", false);
		    $('#caracpc').prop("disabled", false);
		    $('#lignepc').prop("disabled", false);
		    $('#quantitepc').prop("disabled", false);

		    $('#nbpc').html(data.documentElement.getAttribute("nombre"));

		    $('#confirm').html('<div class="alert alert-success">Porte-clef ajouté avec succès !</div>');
		    setTimeout("$('confirm').innerHTML = '';", 3500);

		    $('#nompc').focus();
		}
	    }
	}).fail(function(jqXHR, textStatus) {
	    printState(7, textStatus);
	});

	if(modif)
	{
	    lign = $('#lignepc').val();
	    $('#contenu').html("<h2 class=\"vp\">Veuillez patienter, modification du porte-clef en cours ...</h2>");
	}
    }
}

function GSPC_viewliste()
{
    var pagination = document.createElement("div");
    pagination.className = "pagination top";
    pagination.id = "pagination1";
    $('#contenu').append(pagination);

    var tableau = document.createElement("div");
    tableau.className = "table cols3";
    tableau.id = "list";
        var tableau_head = document.createElement("div");
        tableau_head.className = "head";
    	    var head_th =  document.createElement("span");
    	    head_th.innerHTML = "Nom/Marque";
    	    head_th.onclick = function(){ GSPC_showliste(1, 0); }
    	    tableau_head.appendChild(head_th);
    	    var head_th =  document.createElement("span");
    	    head_th.innerHTML = "Caractéristique";
    	    head_th.onclick = function(){ GSPC_showliste(2, 0); }
    	    tableau_head.appendChild(head_th);
    	    var head_th =  document.createElement("span");
    	    head_th.innerHTML = "Ligne";
    	    head_th.onclick = function(){ GSPC_showliste(3, 0); }
    	    tableau_head.appendChild(head_th);
    	tableau.appendChild(tableau_head);
    	var tableau_body = document.createElement("div");
    	    tableau_body.className = "body";
    	    tableau_body.id = "bodyList";
    	    tableau_body.innerHTML = '<div class="lign elt0 foot">Chargement de la liste en cours ... <img src="applications/GSPC/loader.gif" alt="Veuillez patienter"></div>';
    	tableau.appendChild(tableau_body);
    $('#contenu').append(tableau);

    var pagination = document.createElement("div");
    pagination.className = "pagination bottom";
    pagination.id = "pagination2";
    $('#contenu').append(pagination);

    GSPC_showliste(-1, 0);
}

function GSPC_showliste(tri, page)
{
    if (GSPC_origin_liste == false)
        setTimeout(GSPC_showliste, 200, tri);
    else
    {
        if (GSPC_liste == false)
            GSPC_liste = GSPC_origin_liste;

        //On tente un tri
        if (GSPC_lastSort != tri)
        {
            $('#bodyList').html('<div class="lign elt0 foot">Tri de la liste en cours ...</div>');
            GSPC_liste = GSPC_liste.sort(function(a,b){return a[tri].localeCompare(b[tri]);});
            GSPC_lastSort = tri;
        }

        nbPages = Math.ceil(GSPC_liste.length / nbParPage);
        $('#pagination1').html("");
        $('#pagination2').html("");
        for (var i = 0; i < 4 && i < nbPages; i++)
            GSPC_addLinkPagination(i);
        if (page > 5 && i < nbPages)
        {
            $('#pagination1').append("...");
            $('#pagination2').append("...");
        }
        for (var i = (page < 6?4:page - 2); (i < page + 3 && i < nbPages); i++)
            GSPC_addLinkPagination(i);
        if (page <= 5 && i < nbPages)
        {
            $('#pagination1').append("...");
            $('#pagination2').append("...");
            for (var i = Math.ceil(nbPages/2) - 2; i < nbPages/2 + 2; i++)
                GSPC_addLinkPagination(i);
        }
        if (page + 3 < nbPages - 4)
        {
            $('#pagination1').append("...");
            $('#pagination2').append("...");
        }
        for (var i = (page + 3 >= nbPages - 4?page + 3:nbPages - 4); (i < nbPages); i++)
            GSPC_addLinkPagination(i);

        start = page * nbParPage;
        $('#bodyList').html("");

        for (var i = 0; i < nbParPage && i + start < GSPC_liste.length; i++)
        {
            var lign = document.createElement("div");
            lign.className = "elt" + (i%2);
            GSPC_addLinkPC(lign, i + start);
                var col = document.createElement("span");
                col.innerHTML = GSPC_liste[i + start][1];
                lign.appendChild(col);
                var col = document.createElement("span");
                col.innerHTML = GSPC_liste[i + start][2];
                lign.appendChild(col);
                var col = document.createElement("span");
                col.innerHTML = GSPC_liste[i + start][3];
                lign.appendChild(col);
            $('#bodyList').append(lign);
        }
        lign.className += " foot"
    }
}

function GSPC_addLinkPagination(page)
{
    var lnkP = document.createElement("a");
    lnkP.href = "javascript:GSPC_showliste(" + GSPC_lastSort + ", " + page + ");";
    lnkP.innerHTML = page + 1;
    $('#pagination1').append(lnkP);
    var lnkP = document.createElement("a");
    lnkP.href = "javascript:GSPC_showliste(" + GSPC_lastSort + ", " + page + ");";
    lnkP.innerHTML = page + 1;
    $('#pagination2').append(lnkP);
}

function GSPC_addLinkPC(elt, idPC)
{
    elt.onclick = function() {
	$('#contenu').html('<h2>Modification d\'un porte-clef</h2>');
    
	GSPC_addScreen(idPC + 1);
    }
}

function GSPC_apropos()
{
    aproposApp("GSPC", "contenu");
}