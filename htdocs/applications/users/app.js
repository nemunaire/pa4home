var USERS_liste = false;

function USERS_add()
{
	$('corps').innerHTML = "";
	USERS_display("add", false);
}

function USERS_modif(id)
{
	$('corps').innerHTML = "";
	if (!USERS_liste)
		USERS_loadListe(false);

	USERS_display("modif", USERS_liste[id]);
}

function USERS_delete(id)
{
	alert("Demande de suppression de l'utilisateur : " + id);
}

function USERS_loadListe(async)
{
	new Ajax.Request(
		'ajax.php',
			{
			method: 'get',
			asynchronous: async,
			parameters: {d: "action", a: "users"},
			onSuccess: function(transport, json)
				{
					USERS_liste = USERS_liste;
				},
			onFailure: function() { printEtat(3); }
		}
	);
}

function USERS_display(page, plus)
{
	alert('pom');
	if (page == "add" || page == "modif")
	{
		formMod = document.createElement("form");
		if (plus)
			formMod.onsubmit = function() { USERS_sendModif(plus.id); return false; }
		else
			formMod.onsubmit = USERS_sendAdd;

			fieldMod = document.createElement("fieldset");
			fieldMod.style.width = "500px";
			fieldMod.style.margin = "auto";
				fieldleg = document.createElement("legend");
				fieldleg.innerHTML = "Utilisateur";
				fieldMod.appendChild(fieldleg);

				labelchamp = document.createElement("label");
				labelchamp.innerHTML = "Nom :";
				labelchamp.setAttribute("for", "nom");
				fieldMod.appendChild(labelchamp);
					fieldMod.innerHTML += " ";
				inputchamp = document.createElement("input");
				inputchamp.type = "text";
				inputchamp.id = "nom";
				if (plus)
					inputchamp.value = plus.pseudo;
				fieldMod.appendChild(inputchamp);

				fieldMod.appendChild(document.createElement("br"));

				labelchamp = document.createElement("label");
				labelchamp.innerHTML = "Mot de passe :";
				labelchamp.setAttribute("for", "mdp");
				fieldMod.appendChild(labelchamp);
					fieldMod.innerHTML += " ";
				inputchamp = document.createElement("input");
				inputchamp.type = "password";
				inputchamp.id = "mdp";
				fieldMod.appendChild(inputchamp);

				fieldMod.appendChild(document.createElement("br"));

				labelchamp = document.createElement("label");
				labelchamp.innerHTML = "Confirmer mot de passe :";
				labelchamp.setAttribute("for", "conf");
				fieldMod.appendChild(labelchamp);
					fieldMod.innerHTML += " ";
				inputchamp = document.createElement("input");
				inputchamp.type = "password";
				inputchamp.id = "conf";
				fieldMod.appendChild(inputchamp);

				fieldMod.appendChild(document.createElement("br"));
				fieldMod.appendChild(document.createElement("br"));

				labelchamp = document.createElement("label");
				labelchamp.innerHTML = "Adresse électronique :";
				labelchamp.setAttribute("for", "mail");
				fieldMod.appendChild(labelchamp);
					fieldMod.innerHTML += " ";
				inputchamp = document.createElement("input");
				inputchamp.type = "text";
				if (plus)
					inputchamp.value = plus.mail;
				inputchamp.id = "mail";
				fieldMod.appendChild(inputchamp);

				fieldMod.appendChild(document.createElement("br"));
				fieldMod.appendChild(document.createElement("br"));

				inputchamp = document.createElement("input");
				inputchamp.type = "submit";
				inputchamp.value = "Continuer";
				fieldMod.appendChild(inputchamp);
			formMod.appendChild(fieldMod);
		$('corps').appendChild(formMod);
	}
	else
	{
		if (plus)
			USERS_liste = plus.users;

		tableMessages = document.createElement("table");
		tableMessages.style.margin = "auto";
		tableMessages.style.width = "90%";
			theMess = document.createElement("thead");
				trMess = document.createElement("tr");
					thMess = document.createElement("th");
					thMess.innerHTML = "Action";
					trMess.appendChild(thMess);
					thMess = document.createElement("th");
					thMess.innerHTML = "Utilisateur";
					trMess.appendChild(thMess);
					thMess = document.createElement("th");
					thMess.innerHTML = "Adresse électronique";
					trMess.appendChild(thMess);
					thMess = document.createElement("th");
					thMess.innerHTML = "Dernière connexion";
					trMess.appendChild(thMess);
					thMess = document.createElement("th");
					thMess.innerHTML = "Dernière IP";
					trMess.appendChild(thMess);
				theMess.appendChild(trMess);
			tableMessages.appendChild(theMess);
				tboMess = document.createElement("tbody");
				tboMess.id = "users";
			tableMessages.appendChild(tboMess);
		$('corps').appendChild(tableMessages);

		nbUsers = USERS_liste.length;
		for(i=0; i<nbUsers; i++)
		{
			var date = new Date();
			date.setTime(USERS_liste[i].last_visite*1000);

			var newRow = $('users').insertRow(-1);
			var newCell = newRow.insertCell(0);
			newCell.innerHTML = '<a href="javascript:USERS_modif(' + i + ');">Modifier</a>';
			var newCell = newRow.insertCell(1);
			newCell.innerHTML = USERS_liste[i].pseudo;
			var newCell = newRow.insertCell(2);
			newCell.innerHTML = USERS_liste[i].mail;
			var newCell = newRow.insertCell(3);
			newCell.innerHTML = (date.getDay()<10?"0"+date.getDay():date.getDay()) + "/" + (date.getMonth()<10?"0"+date.getMonth():date.getMonth()) + "/" + date.getFullYear() + " " + (date.getHours()<10?"0"+date.getHours():date.getHours()) + ":" + (date.getMinutes()<10?"0"+date.getMinutes():date.getMinutes()) + ":" + (date.getSeconds()<10?"0"+date.getSeconds():date.getSeconds());
			var newCell = newRow.insertCell(4);
			newCell.innerHTML = USERS_liste[i].last_ip;
		}

		var newRow = $('users').insertRow(-1);
		var newCell = newRow.insertCell(0);
		newCell.innerHTML = '<a href="javascript:USERS_add();">Ajouter</a>';
	}
}

function USERS_sendAdd()
{
	if ($('mdp').value.length < 5)
		alert("Le mot de passe est trop court !\nIl doit faire au moins 5 caractères.");
	else if ($('mdp').value != $('conf').value)
		alert("Le mot de passe est sa confirmation est différent !");
	else
	{
		new Ajax.Request(
			'ajax.php?d=action&a=users',
				{
				method: 'post',
				asynchronous: async,
				parameters: {nom: $('nom').value, mdp: $('mdp').value, mail: $('mail').value},
				onSuccess: function(transport, json)
					{
						USERS_liste = json.users;
					},
				onFailure: function() { printEtat(3); }
			}
		);
	}

	return false;
}

function USERS_sendModif(id)
{
	if ($('mdp').value.length < 5)
		alert("Le mot de passe est trop court !\nIl doit faire au moins 5 caractères.");
	else if ($('mdp').value != $('conf').value)
		alert("Le mot de passe est sa confirmation est différent !");
	else
	{
		alert("Demande de modification pour l'utilisateur ayant l'ID : " + id);


	}

	$('mdp').value = "";
	$('conf').value = "";
}