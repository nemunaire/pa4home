function Album(elt)
{
    this.id = elt.getAttribute("id");
    this.type = parseInt(elt.getAttribute("type"));
    this.titre = elt.getAttribute("titre");
    if (elt.hasAttribute("couleur"))
	this.color = elt.getAttribute("couleur");
}
