function Chanson(elt)
{
    this.albums = new Array();
    if (elt)
    {
	this.id = elt.getAttribute("id");
	this.titre = elt.getAttribute("titre");
	this.artiste = elt.getAttribute("artiste");

	for (var albm in elt.getElementsByTagName("inalbum"))
	{
	    if (albm.isInt())
		this.albums.push(elt.getElementsByTagName("inalbum")[albm].getAttribute("id"));
	}
    }
}

Chanson.prototype.inAlbum = function(idAlbum, types)
{
    for (var i = 0; i < this.albums.length; i++)
    {
	if (
            (typeof(idAlbum) == 'undefined' || this.albums[i] == idAlbum) &&
            (typeof(types) == 'undefined' || types.in_array(GSM_liste_CDs[this.albums[i]].type))
        )
	    return true;
    }
    return false;
}

Chanson.prototype.search = function(titre, artiste, album, types)
{
    if (
        (titre == '' || this.titre.toLowerCase().indexOf(titre.toLowerCase()) >= 0) &&
        (artiste == '' || this.artiste.toLowerCase().indexOf(artiste.toLowerCase()) >= 0)
       )
    {
        for (var i = 0; i < this.albums.length; i++)
        {
	    if (album == '' || GSM_liste_CDs[this.albums[i]].titre.toLowerCase().indexOf(album.toLowerCase()) >= 0)
	        return (types.length == 0 || types.in_array(GSM_liste_CDs[this.albums[i]].type));
        }
    }
    return false;
}
