﻿var GSM_origin_liste = false;
var GSM_liste = false;
var GSM_liste_CDs = false; //Albums, tout type
var GSM_lastSort = -1;
var GSM_delay = null;
var nbParPage = 50;

//On charge la liste des porte-clés
function GSM_loadList()
{
    GSM_origin_liste = false;
    GSM_liste = false;
    GSM_liste_CDs = false;
    GSM_lastSort = -1;
    $('#nbpc').html('<img src="applications/GSM/loader.gif" alt="Chargement en cours ...">');

    $.ajax({
	url: "ajax.php",
	data: {d: "action", a: "GSM", p: "liste"},
    }).done(GSM_parseList).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}
function GSM_loadSelectedList()
{
    //TODO: ne charger que les listes dont l'état est false
    GSM_loadList();
}
GSM_loadList();

//Une fois la liste reçue, on la parse
function GSM_parseList(data)
{
    $('#nbpc').html(data.documentElement.getAttribute("nombre"));

    GSM_origin_liste = new Array();
    var liste = data.documentElement.getElementsByTagName("chanson");
    for (var elt in liste)
    {
	if (elt.isInt())
	{
	    var chan = new Chanson(liste[elt]);
	    GSM_origin_liste[chan.id] = chan;
	}
    }

    GSM_liste_CDs = new Array();
    liste = data.documentElement.getElementsByTagName("album");
    for (var elt in liste)
    {
	if (elt.isInt())
	{
	    var alb = new Album(liste[elt]);
	    GSM_liste_CDs[alb.id] = alb;
	}
    }
}

function GSM_add()
{
    window.scrollTo(0,0);
    $('#contenu').html("<h2>Ajout d'un album à la base de données</h2>");

    GSM_addAlbum();
}

function GSM_edit()
{
    window.scrollTo(0,0);
    $('#contenu').html("<h2>Liste des albums</h2>");

    GSM_viewliste(0, 0, 0);
}

function GSM_listAlbums()
{
    window.scrollTo(0,0);
    $('#contenu').html("<h2>Liste des albums</h2>");

    GSM_viewalbums(false);
}

function GSM_listTitres(types)
{
    window.scrollTo(0,0);
    $('#contenu').html("<h2>Liste des titres</h2>");

    var confirmation = document.createElement("h2");
    confirmation.style.color = "teal";
    confirmation.id = "confirm";
    $('#contenu').append(confirmation);

    GSM_liste = false;

    GSM_viewliste(types, 0, 0);
}

function GSM_rechTitres()
{
    window.scrollTo(0,0);
    $('#contenu').html("<h2>Recherche d'un élément</h2>");

    GSM_formRech("", "", "", [1,2,3,4,5,6,7,8]);
}

function GSM_formRech(titleC, artiste, album, types)
{
    var formulaire = document.createElement("form");
    formulaire.id = "rechTitles";
    formulaire.onsubmit = function() {
        tps = [];
        if ($("#type1").prop("checked")) tps.push(1);
        if ($("#type2").prop("checked")) tps.push(2);
        if ($("#type3").prop("checked")) tps.push(3);
        rechTitles($("#titreC").val(),
                   $("#artiste").val(),
                   $("#album").val(),
                   tps);
        return false;
    }
      var cntr = document.createElement("div");
        var lbl = document.createElement("label");
        lbl.htmlFor = "titreC"; lbl.innerHTML = "Titre :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text"; inpt.name = "titre";
        inpt.value = titleC;
        inpt.id = "titreC"; cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));

        var lbl = document.createElement("label");
        lbl.htmlFor = "artiste"; lbl.innerHTML = "Artiste :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text"; inpt.name = "artiste";
        inpt.value = artiste;
        inpt.id = "artiste"; cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));

        var lbl = document.createElement("label");
        lbl.htmlFor = "album"; lbl.innerHTML = "Album :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text"; inpt.name = "album";
        inpt.value = album;
        inpt.id = "album"; cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));
      formulaire.appendChild(cntr);

      var inpt = document.createElement("input");
      inpt.type = "checkbox"; inpt.name = "type1";
      inpt.id = "type1"; inpt.value = "1";
      inpt.checked = types.in_array(1);
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "type1"; lbl.innerHTML = "Albums d'année";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));

      var inpt = document.createElement("input");
      inpt.type = "checkbox"; inpt.name = "type2";
      inpt.id = "type2"; inpt.value = "1";
      inpt.checked = types.in_array(2);
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "type2"; lbl.innerHTML = "Compilations";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));

      var inpt = document.createElement("input");
      inpt.type = "checkbox"; inpt.name = "type3";
      inpt.id = "type3"; inpt.value = "1";
      inpt.checked = types.in_array(3);
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "type3"; lbl.innerHTML = "Musique classique";
      formulaire.appendChild(lbl);

      var cntr = document.createElement("div");
        formulaire.appendChild(document.createElement("br"));
        formulaire.appendChild(document.createElement("br"));
        var inpt = document.createElement("input");
        inpt.type = "submit";
        inpt.value = "Rechercher";
        cntr.appendChild(inpt);
      formulaire.appendChild(cntr);

    $('#contenu').append(formulaire);
}

function GSM_addAlbum()
{
    var formulaire = document.createElement("form");
    formulaire.id = "addAlbum";
    formulaire.className = "form-horizontal";
    formulaire.onsubmit = function() { newAlbum(); return false; }

    formulaire.innerHTML = '<div class="form-group"><label for="albumtype" class="col-sm-5 control-label">Type d\'album :</label><div class="col-sm-7"><select class="form-control" id="albumtype" onchange="GSM_addAlbum_chColor()"><option value="1">Album d\'année</option><option value="2">Compilation</option><option value="3">Musiques classiques</option></select></div></div>    <div class="form-group"><label for="nomAlbum" class="col-sm-5 control-label">Nom :</label><div class="col-sm-7"><input type="text" id="nomAlbum" maxlength="250" class="form-control"></div></div>    <div class="form-group"><label for="colorAlbum" class="col-sm-5 control-label">Couleur :</label><div class="col-sm-7"><input type="text" id="colorAlbum" maxlength="7" class="form-control"></div></div>    <div class="form-group"><label for="copycolor" class="col-sm-5 control-label">Copier la couleur depuis :</label><div class="col-sm-7"><select class="form-control" id="copyfrom"></select></div></div>    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Ajouter</button>';

    $('#contenu').append(formulaire);

    // TODO: check GSM_liste_CDs arrived
    GSM_addAlbum_chColor();
}

function GSM_addAlbum_chColor()
{
    var type = $('#albumtype').val();

    $("#copyfrom").html('<option value="0"> </option>');

    for (var i in GSM_liste_CDs)
    {
	if (i.isInt() && GSM_liste_CDs[i].type == type)
	{
	    var opt = document.createElement("option");
	    opt.value = i;
	    opt.innerHTML = GSM_liste_CDs[i].titre;
	    $("#copyfrom").append(opt);
	}
    }

    if ($("#copyfrom").html() != "")
	$("#copyfrom").change(function(event) {
	    if (this.value > 0)
		GSM_addAlbum_loadColor(this, type);
	});
}

function GSM_addAlbum_loadColor(elt, type)
{
    $.ajax({
	url: 'ajax.php',
	data: { d: "action", a: "GSM", p: "color", type: type, id: elt.value },
    }).done(function(data) {
	$('#colorAlbum').val(data.documentElement.getElementsByTagName("color")[0].firstChild.textContent);
	$("#testColor").css("background-color", $('#colorAlbum').val());
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

function newAlbum()
{
    var type = $('#albumtype').val();

    var nom = $('#nomAlbum').val();
    if (nom)
    {
	//On ajoute l'album
	$.ajax({
	    url: 'ajax.php?d=action&a=GSM&p=add',
	    type: "POST",
	    data: {
		    type: type,
		    title: escapeHtml(nom),
		    color: $('#colorAlbum').val()
	          },
	}).done(function(data) {
	    if (data.documentElement.getAttribute("statut") != 1)
	    {
		firstLoad();
		alert("Vous avez été déconnecté. L'album' n'a pas été ajouté.");
	    }
	    else
	    {
		if (data.documentElement.getElementsByTagName("id")[0])
		{
		    GSM_liste_CDs = false;
		    
		    $('#contenu').empty();
		    titre = document.createElement("h2");
		    titre.style.color = "#FF8800";
		    titre.innerHTML = 'Chargement en cours ... <img src="applications/GSM/loader.gif" alt="Veuilez patienter">';
		    $('#contenu').append(titre);

		    //On recharge la liste d'album
		    GSM_loadSelectedList();

		    //On passe à l'écran de modification du nouvel album
		    GSM_viewalbum(type,
				  data.documentElement.getElementsByTagName("id")[0].textContent,
				  0,
				  nom);
		}
		else
		    alert("L'ajout de l'album ne s'est pas passé correctement, vérifiez que cet album n'existe pas déjà.");
	    }
	}).fail(function(jqXHR, textStatus) {
	    printState(7, textStatus);
	});
    }
    else
	alert("Vous n'avez pas précisé de nom pour ce nouvel album.");
}

function GSM_addForm(type, idAlbum, modif, page)
{
    //Si le formulaire existe déjà, on ne fait que remplacer son contenu
    var formulaire;
    if ($('#addTitle').length)
    {
	$('#addTitle').empty();
	formulaire = $('#addTitle')[0];
    }
    else
    {
	formulaire = document.createElement("form");
	formulaire.className = "form-horizontal container-fluid";
	formulaire.id = "addTitle";
	$('#contenu').append(formulaire);
    }

    formulaire.className = "form-horizontal";
    formulaire.onsubmit = function() { checkAndAdd(type, idAlbum, modif, page); return false; }
    var th = document.createElement("h3");
    if (modif)
	th.innerHTML = "Modification d'un titre";
    else
	th.innerHTML = "Ajout d'un titre à l'album";

    formulaire.innerHTML = '<div class="form-group"><label for="titrechan" class="col-sm-5 control-label">Titre :</label><div class="col-sm-7"><input type="text" id="titrechan" maxlength="250" class="form-control"></div></div>    <div class="form-group"><label for="auteur" class="col-sm-5 control-label">Interpréte :</label><div class="col-sm-7"><input type="text" id="auteur" maxlength="250" class="form-control"></div></div>';

    if(modif)
    {
	formulaire.innerHTML += '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Modifier</button>';

	inp_del = document.createElement("button");
	inp_del.type = "button";
	inp_del.className = "btn btn-danger";
	inp_del.innerHTML = '<span class="glyphicon glyphicon-trash"></span> Supprimer';
	inp_del.style.marginLeft = "15px";
	inp_del.onclick = function() { GSM_del(type, idAlbum, modif, page); };
	formulaire.appendChild(inp_del);

	$("#titrechan").val(GSM_liste[modif-1].titre);
	$("#auteur").val(GSM_liste[modif-1].artiste);
	$('#titrechan').focus();
    }
    else
	formulaire.innerHTML += '<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Ajouter</button>';

/*    new Ajax.Autocompleter(
        "auteur",
        "auteur_propositions",
        "applications/GSM/auteurs.php",
        {
            paramName: 'auteur',
            minChars: 1
        });*/
}

function GSM_checkChange(e)
{
    if (this.value == "add")
    {
	if (this.id == "discAuteur")
	    id_name = "new_auteur";
	else if (this.id == "discDece")
	    id_name = "new_decenie";
	else if (this.id == "discAnnee")
	    id_name = "new_annee";

	document.getElementById(id_name).style.display = "inline";
	if (document.getElementById(id_name).value == "")
	    document.getElementById(id_name).value = "Nom du nouvel l'album";
	document.getElementById(id_name).focus();
	document.getElementById(id_name).select();
    }
    else if (this.id == "discAuteur")
	document.getElementById("new_auteur").style.display = "none";
    else if (this.id == "discDece")
	document.getElementById("new_decenie").style.display = "none";
    else if (this.id == "discAnnee")
	document.getElementById("new_annee").style.display = "none";
}

function GSM_del(type, idAlbum, modif, page)
{
    $('#titrechan').prop("disabled", true);
    $('#auteur').prop("disabled", true);

    var identifiant = GSM_liste[modif-1].id;

    $.ajax({
	url: 'ajax.php?d=action&a=GSM&p=del',
	method: 'post',
	parameters: {
	    id: identifiant,
	    alb: idAlbum
	}
    }).done(function(data) {
	if (data.documentElement.getAttribute("statut") != 1)
	{
	    firstLoad();
	    alert("Vous avez été déconnecté. La chanson n'a pas été enregistrée");
	}
	else
	{
	    delete GSM_origin_liste[identifiant];

	    setTimeout('$("#confirm").html("Chanson supprimée avec succès !");', 234);
	    setTimeout("$('#confirm').html('');", 3500);
	    
	    $('#nbpc').html(data.documentElement.getAttribute("nombre"));
	    
	    if (type)
		GSM_viewalbum(type, idAlbum, page);
	    else
		GSM_viewliste(type, 0, page);
	    
	    if (modif)
		window.scrollTo(0,0);
	}
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });

    //On réinitialise les variables de liste pour aller rechercher les listes
    GSM_liste = false;
    GSM_pagination = false;
    $('#contenu').html("<h2 class=\"vp\" id=\"confirm\">Veuillez patienter suppression de la chanson en cours ...</h2>");
}

function checkAndAdd(type, idAlbum, modif, page)
{
    var modifURI = "";
    if(modif)
	modifURI = "&id=" + GSM_liste[modif-1].id;

    $('#titrechan').parent().parent().removeClass("has-error");
    $('#auteur').parent().parent().removeClass("has-error");
    if ($('#titrechan').val() == "")
    {
	$('#titrechan').parent().parent().addClass("has-error");
	alert('Vous n\'avez pas indiqué le titre de la chanson !');
    }
    else if ($('#auteur').val() == "")
    {
	$('#auteur').parent().parent().addClass("has-error");
	alert('Vous n\'avez pas indiqué l\'auteur de la chanson !');
    }
    else
    {
	$('#titrechan').prop("disabled", true);
	$('#auteur').prop("disabled", true);

	$.ajax({
	    url: 'ajax.php?d=action&a=GSM' + modifURI,
	    type: 'POST',
	    data: {
		titre: escapeHtml($('#titrechan').val()),
		auteur: escapeHtml($('#auteur').val()),
		type: type,
		alb: idAlbum
	    }
	}).done(function(data) {
	    if (data.documentElement.getAttribute("statut") != 1)
	    {
		firstLoad();
		alert("Vous avez été déconnecté. La chanson n'a pas été enregistrée");
	    }
	    else
	    {
		if (data.documentElement.getElementsByTagName("id").length == 0)
		{
		    setTimeout('$("#confirm").html("Une erreur s\'est produite lors de l\'ajout de la chanson, veuillez recommencer.");', 234);
		    setTimeout("$('#confirm').empty();", 3500);
		}
		else if(modif)
		{
		    setTimeout('$("#confirm").html("Chanson modifiée avec succès !");', 234);
		    setTimeout("$('#confirm').empty();", 3500);
		    
		    GSM_liste[modif-1].titre = $('#titrechan').val();
		    GSM_liste[modif-1].artiste = $('#auteur').val();
		}
		else
		{
		    nc = new Chanson();
		    nc.id = data.documentElement.getElementsByTagName("id")[0].textContent;
		    nc.titre = $('#titrechan').val();
		    nc.artiste = $('#auteur').val();
		    nc.albums.push(idAlbum);
		    GSM_origin_liste[nc.id] = nc;
		    GSM_liste.push(nc);
		    
		    $('#titrechan').val("");
		    $('#auteur').val("");
		    
		    $('#titrechan').prop("disabled", false);
		    $('#auteur').prop("disabled", false);
		    
		    $('#nbpc').html(data.documentElement.getAttribute("nombre"));
		    
		    setTimeout('$("#confirm").html("Chanson ajoutée avec succès !");', 234);
		    setTimeout("$('#confirm').empty();", 3500);
		}

		if (idAlbum)
		    GSM_viewalbum(type, idAlbum, page);
		else
		    //TODO à tester ça marche pas
		    GSM_viewliste(type, 0, page);
		
		if (modif)
		    window.scrollTo(0,0);
		else
		    $('#titrechan').focus();
		    }
	}).fail(function(jqXHR, textStatus) {
	    printState(7, textStatus);
	});
    }
}

function GSM_viewalbums()
{
    if (GSM_origin_liste == false)
	setTimeout(GSM_viewalbums, 200);
    else
    {
	var tableau = document.createElement("div");
	tableau.className = "table cols2 stats";
	tableau.id = "statsA";
	var tableau_head = document.createElement("div");
	tableau_head.className = "head";
	var head_th =  document.createElement("span");
	head_th.innerHTML = "CD années";
	tableau_head.appendChild(head_th);
	var head_th =  document.createElement("span");
	head_th.innerHTML = "Nombre";
	tableau_head.appendChild(head_th);
	tableau.appendChild(tableau_head);
	var tableau_bodyA = document.createElement("div");
	tableau_bodyA.className = "body";
	tableau_bodyA.innerHTML = '<div class="lign elt0 foot">Chargement de la liste ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
	tableau.appendChild(tableau_bodyA);
	$('#contenu').append(tableau);

	var tableau = document.createElement("div");
	tableau.className = "table cols2 stats";
	tableau.id = "statsD";
	var tableau_head = document.createElement("div");
	tableau_head.className = "head";
	var head_th =  document.createElement("span");
	head_th.innerHTML = "CD décénies";
	tableau_head.appendChild(head_th);
	var head_th =  document.createElement("span");
	head_th.innerHTML = "Nombre";
	tableau_head.appendChild(head_th);
	tableau.appendChild(tableau_head);
	var tableau_bodyD = document.createElement("div");
	tableau_bodyD.className = "body";
	tableau_bodyD.innerHTML = '<div class="lign elt0 foot">Chargement de la liste ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
	tableau.appendChild(tableau_bodyD);
	$('#contenu').append(tableau);

	var tableau = document.createElement("div");
	tableau.className = "table cols2 stats";
	tableau.id = "statsC";
	var tableau_head = document.createElement("div");
	tableau_head.className = "head";
	var head_th =  document.createElement("span");
	head_th.innerHTML = "CD classiques";
	tableau_head.appendChild(head_th);
	var head_th =  document.createElement("span");
	head_th.innerHTML = "Nombre";
	tableau_head.appendChild(head_th);
	tableau.appendChild(tableau_head);
	var tableau_bodyC = document.createElement("div");
	tableau_bodyC.className = "body";
	tableau_bodyC.innerHTML = '<div class="lign elt0 foot">Chargement de la liste ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
	tableau.appendChild(tableau_bodyC);
	$('#contenu').append(tableau);

	$.ajax({
	    url: 'ajax.php',
	    data: {d: "action", a: "GSM", p: "stats"},
	}).done(function(data) {
	    var annees = data.documentElement.getElementsByTagName("annee");
	    var chanteurs = data.documentElement.getElementsByTagName("interprete");
	    var decenies = data.documentElement.getElementsByTagName("decenie");

	    tableau_bodyA.innerHTML = "";
	    for (var i = 0; i < annees.length; i++)
	    {
		if (annees[i].getAttribute("id") != "")
		{
		    var lign = document.createElement("div");
		    lign.className = "elt" + (i%2);
		    GSM_addStatLign(lign, [1], annees[i].getAttribute("id"));
		    var col = document.createElement("span");
		    col.innerHTML = GSM_liste_CDs[annees[i].getAttribute("id")].titre;
		    lign.appendChild(col);
		    var col = document.createElement("span");
		    col.innerHTML = annees[i].getAttribute("nbTitles");
		    lign.appendChild(col);
		    tableau_bodyA.appendChild(lign);
		}
	    }
	    if (annees.length == 0)
		tableau_bodyA.innerHTML = '<div class="lign elt0 foot">Aucun CD</div>';
	    else
		lign.className += " foot";

	    tableau_bodyC.innerHTML = "";
	    for (var i = 0; i < chanteurs.length; i++)
	    {
		if (chanteurs[i].getAttribute("id") != "")
		{
		    var lign = document.createElement("div");
		    lign.className = "elt" + (i%2);
		    GSM_addStatLign(lign, [2], chanteurs[i].getAttribute("id"));
		    var col = document.createElement("span");
		    col.innerHTML = GSM_liste_CDs[chanteurs[i].getAttribute("id")].titre;
		    lign.appendChild(col);
		    var col = document.createElement("span");
		    col.innerHTML = chanteurs[i].getAttribute("nbTitles");
		    lign.appendChild(col);
		    tableau_bodyC.appendChild(lign);
		}
	    }
	    if (chanteurs.length == 0)
		tableau_bodyC.innerHTML = '<div class="lign elt0 foot">Aucun CD</div>';
	    else
		lign.className += " foot";

	    tableau_bodyD.innerHTML = "";
	    for (var i = 0; i < decenies.length; i++)
	    {
		if (decenies[i].getAttribute("id") != "")
		{
		    var lign = document.createElement("div");
		    lign.className = "elt" + (i%2);
		    GSM_addStatLign(lign, [3], decenies[i].getAttribute("id"));
		    var col = document.createElement("span");
		    col.innerHTML = GSM_liste_CDs[decenies[i].getAttribute("id")].titre;
		    lign.appendChild(col);
		    var col = document.createElement("span");
		    col.innerHTML = decenies[i].getAttribute("nbTitles");
		    lign.appendChild(col);
		    tableau_bodyD.appendChild(lign);
		}
	    }
	    if (decenies.length == 0)
		tableau_bodyD.innerHTML = '<div class="lign elt0 foot">Aucun CD</div>';
	    else
		lign.className += " foot";
	}).fail(function(jqXHR, textStatus) {
	    printState(7, textStatus);
	});
    }
}

function GSM_addStatLign(lign, type, id)
{
    lign.onclick = function() { window.scrollTo(0,0); GSM_viewalbum(type, id, 0) };
}

function GSM_viewalbum(type, id, page, title)
{
    window.scrollTo(0,75);
    $('#contenu').html("<h2 id=\"confirm\"></h2>");

    var titre = document.createElement("h2");
    if (typeof(title) == 'undefined')
	titre.innerHTML = "Liste des titres de " + GSM_liste_CDs[id].titre;
    else
	titre.innerHTML = "Liste des titres de " + title;
    $('#contenu').append(titre);
    GSM_delay_viewalbum(type, id, page);
}

function GSM_build_liste(id, types)
{
    GSM_liste = new Array();
    for (var i in GSM_origin_liste)
    {
	if (i.isInt() && typeof(GSM_origin_liste[i]) != 'undefined')
	{
	    if (GSM_origin_liste[i].inAlbum(id, types))
		GSM_liste.push(GSM_origin_liste[i]);
	}
    }
}

function rechTitles(title, artiste, album, types)
{
    if (GSM_origin_liste == false)
	GSM_delay = setTimeout(rechTitles, titre, artiste, album, types);
    else
    {
        window.scrollTo(0,0);
        $('#contenu').innerHTML = "";
        titre = document.createElement("h2");
        titre.className = "vp";
        titre.innerHTML = "Recherche en cours ...<small>Veuillez patienter</small>";
        $('#contenu').append(titre);

        GSM_liste = new Array();
        for (var i in GSM_origin_liste)
        {
	    if (i.isInt() && typeof(GSM_origin_liste[i]) != 'undefined')
	    {
	        if (GSM_origin_liste[i].search(title, artiste, album, types))
		    GSM_liste.push(GSM_origin_liste[i]);
	    }
        }

        $('#contenu').html("");
        titre = document.createElement("h2");
        titre.innerHTML = "Recherche d'un élément";
        $('#contenu').append(titre);

        if (GSM_liste == false)
        {
            titre2 = document.createElement("h3");
            titre2.innerHTML = "Aucun élément n'a été trouvé avec ces critères";
            $('#contenu').append(titre2);

            GSM_formRech(title, artiste, album, types);
        }
        else
	    GSM_viewliste(0, 0, 0);
   }
}

function GSM_delay_viewalbum(type, id, page)
{
    if (GSM_origin_liste == false)
	GSM_delay = setTimeout(GSM_delay_viewalbum, 200, type, id, page);
    else
    {
	GSM_build_liste(id);
	if (GSM_liste == false)
	    GSM_liste[0] = 2;

	GSM_lastSort = -1;

	GSM_viewliste(type, id, page);

	GSM_addForm(type, id, false, 0);
    }
}

function GSM_viewliste(type, idAlbum, page)
{
    var pagination = document.createElement("div");
  pagination.className = "pagination top";
    pagination.id = "pagination1";
    $('#contenu').append(pagination);

    var tableau = document.createElement("div");
    tableau.className = "table cols3";
    tableau.id = "list";
      var tableau_head = document.createElement("div");
      tableau_head.className = "head";
        var head_th =  document.createElement("span");
        head_th.innerHTML = "Titre";
        head_th.onclick = function() { GSM_showliste("titre", 0, type, idAlbum); }
        tableau_head.appendChild(head_th);
        var head_th =  document.createElement("span");
        head_th.innerHTML = "Chanteur";
        head_th.onclick = function() { GSM_showliste("artiste", 0, type, idAlbum); }
        tableau_head.appendChild(head_th);
        var head_th =  document.createElement("span");
        head_th.innerHTML = "CDs";
        head_th.style.cursor = "auto";
        tableau_head.appendChild(head_th);
      tableau.appendChild(tableau_head);
      var tableau_body = document.createElement("div");
        tableau_body.className = "body";
        tableau_body.id = "bodyList";
        tableau_body.innerHTML = '<div class="lign elt0 foot">Chargement de la liste en cours ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
      tableau.appendChild(tableau_body);
    $('#contenu').append(tableau);


    var pagination = document.createElement("div");
    pagination.className = "pagination bottom";
    pagination.id = "pagination2";
    $('#contenu').append(pagination);

    GSM_showliste(-1, page, type, idAlbum);
}

function GSM_showliste(tri, page, type, idAlbum)
{
    if (GSM_origin_liste == false)
	setTimeout(GSM_showliste, 200, tri, page, type, idAlbum);
    else
    {
	if (GSM_liste == false)
	    GSM_build_liste(undefined, type);

	//On tente un tri
	if (GSM_lastSort != tri && tri != -1)
	{
	    $('#bodyList').html('<div class="lign elt0 foot">Tri de la liste en cours ...</div>');
	    GSM_liste = GSM_liste.sort(function(a,b){return a[tri].localeCompare(b[tri]);});
	    GSM_lastSort = tri;
	}

	nbPages = Math.ceil(GSM_liste.length / nbParPage);
	$('#pagination1').empty();
	$('#pagination2').empty();

	if (nbPages < 3)
	{
	    var lnkP = document.createElement("a");
	    lnkP.href = "javascript:GSM_printliste('" + GSM_lastSort + "', " + type + ", " + idAlbum + ");";
	    lnkP.innerHTML = "Imprimer la pochette";
	    $('#pagination1').append(lnkP);
	    var lnkP = document.createElement("a");
	    lnkP.href = "javascript:GSM_printliste('" + GSM_lastSort + "', " + type + ", " + idAlbum + ");";
	    lnkP.innerHTML = "Imprimer la pochette";
	    $('#pagination2').append(lnkP);
	}

	for (var i = 0; i < 4 && i < nbPages; i++)
	    GSM_addLinkPagination(i, type, idAlbum);
	if (page > 5 && i < nbPages)
	{
	    $('#pagination1').append("...");
	    $('#pagination2').append("...");
	}
	for (var i = (page < 6?4:page - 2); (i < page + 3 && i < nbPages); i++)
	    GSM_addLinkPagination(i, type, idAlbum);
	if (page <= 5 && i < nbPages)
	{
	    $('#pagination1').append("...");
	    $('#pagination2').append("...");
	    for (var i = Math.ceil(nbPages/2) - 2; i < nbPages/2 + 2; i++)
		GSM_addLinkPagination(i, type, idAlbum);
	}
	if (page + 3 < nbPages - 4)
	{
	    $('#pagination1').append("...");
	    $('#pagination2').append("...");
	}
	for (var i = (page + 3 >= nbPages - 4?page + 3:nbPages - 4); (i < nbPages); i++)
	    GSM_addLinkPagination(i, type, idAlbum);

	start = page * nbParPage;
	$('#bodyList').empty();

	if (GSM_liste[0] != 2)
	{
	    for (var i = 0; i < nbParPage && i + start < GSM_liste.length; i++)
	    {
		var lign = document.createElement("div");
		lign.className = "elt" + (i%2);

		GSM_addLinkCD(lign, i + start, idAlbum, type, page);
		var col = document.createElement("span");
		col.innerHTML = GSM_liste[i + start].titre;
		lign.appendChild(col);
		var col = document.createElement("span");
		col.innerHTML = GSM_liste[i + start].artiste;
		lign.appendChild(col);
		var col = document.createElement("span");

		for (var alb in GSM_liste[i + start].albums)
		{
		    if (alb.isInt())
			col.innerHTML += '<a href="javascript:GSM_viewalbum([1,2,3], ' + GSM_liste_CDs[GSM_liste[i + start].albums[alb]].id + ", 0)\">" + GSM_liste_CDs[GSM_liste[i + start].albums[alb]].titre + "</a><br>";
		}
		lign.appendChild(col);
		$('#bodyList').append(lign);
	    }
	}
	else
	{
	    var lign = document.createElement("div");
	    lign.className = "elt0";
	    lign.innerHTML = "Il n'y a aucun titre enregistré dans cet album.";
	    $('#bodyList').append(lign);
	}
	lign.className += " foot";
    }
}
function GSM_addLinkPagination(page, type, idAlbum)
{
	var lnkP = document.createElement("a");
	lnkP.href = "javascript:GSM_showliste('" + GSM_lastSort + "', " + page + ", " + type + ", " + idAlbum + ");";
	lnkP.innerHTML = page+1;
	$('#pagination1').append(lnkP);
	var lnkP = document.createElement("a");
	lnkP.href = "javascript:GSM_showliste('" + GSM_lastSort + "', " + page + ", " + type + ", " + idAlbum + ");";
	lnkP.innerHTML = page+1;
	$('#pagination2').append(lnkP);
}

function GSM_printliste(tri, type, idAlbum)
{
    if (GSM_origin_liste == false)
	setTimeout(GSM_printliste, 200, tri, type, idAlbum);
    else
    {
	$('#contenu').empty();

        var patt1 = "";
        if (GSM_liste_CDs[idAlbum].titre.toLowerCase().indexOf("cd") >= 0)
	    patt1 = GSM_liste_CDs[idAlbum].titre.match(/cd ?([0-9]+)/i).toString();
	var trueTitleL1 = GSM_liste_CDs[idAlbum].titre;
	trueTitleL1 = trueTitleL1.substr(0, trueTitleL1.length - patt1.indexOf(','));
	if (type == 1)
	    trueTitleL1 = "Année " + trueTitleL1;

	var trueTitleL2;
	if (patt1)
	    trueTitleL2 = "CD " + patt1.substr(patt1.indexOf(',')+1);
	else
	    trueTitleL2 = "";

	var trueTitle = trueTitleL1 + " &ndash; " + trueTitleL2;

	var front = document.createElement("div");
	front.className = "front";
	var titleP = document.createElement("div");
	titleP.className = "title";
	titleP.innerHTML = trueTitleL1;
	front.appendChild(titleP);
	var cd = document.createElement("div");
	cd.className = "cd";
	cd.innerHTML = trueTitleL2;
	if (trueTitleL2)
	    cd.style.marginTop = "70px";
	front.appendChild(cd);
	$('#contenu').append(front);

	$.ajax({
	    url: 'ajax.php',
	    data: {d: "action", a: "GSM", p: "color", type: type, id: idAlbum}
	}).done(function(data) {
	    var colors = data.documentElement.getElementsByTagName("color");
	    if (colors.length == 2)
	    {
		titleP.style.color = colors[1].firstChild.textContent;
		cd.style.color = colors[0].firstChild.textContent;
	    }
	    else if (colors.length == 1)
	    {
		titleP.style.color = colors[0].firstChild.textContent;
		cd.style.color = colors[0].firstChild.textContent;
	    }
	}).fail(function(jqXHR, textStatus) {
	    printState(7, textStatus);
	});

	var sp = document.createElement("div");
	sp.className = "sp";
	$('#contenu').append(sp);

	var back = document.createElement("div");
	back.className = "back";
	var backL = document.createElement("div");
	backL.className = "left";
	var title = document.createElement("div");
	title.className = "title";
	title.innerHTML = trueTitleL1 + " " + trueTitleL2;
	backL.appendChild(title);
	back.appendChild(backL);
	var backR = document.createElement("div");
	backR.className = "right";
	var title = document.createElement("div");
	title.className = "title";
	title.innerHTML = trueTitleL1 + " " + trueTitleL2;
	backR.appendChild(title);
	back.appendChild(backR);

	var backC = document.createElement("div");
	backC.className = "center";
	var title = document.createElement("div");
	title.className = "title";
	title.innerHTML = trueTitleL1 + " &ndash; " + trueTitleL2;
	backC.appendChild(title);

	var listeC = document.createElement("ol");
	backC.appendChild(listeC);

	back.appendChild(backC);
	$('#contenu').append(back);

	if (GSM_liste == false)
	{
	    GSM_liste = new Array();
	    for (var i in GSM_origin_liste)
	    {
		if (i.isInt())
		    GSM_liste.push(GSM_origin_liste[i]);
	    }
	}

	//On tente un tri
	if (GSM_lastSort != tri)
	{
	    GSM_liste = GSM_liste.sort(function(a,b){return a[tri].localeCompare(b[tri]);});
	    GSM_lastSort = tri;
	}

	if (GSM_liste[0] != 2)
	{
	    for (var i = 0; i < GSM_liste.length; i++)
	    {
		var lign = document.createElement("li");
		lign.className = "elt" + (i%2);
		var col = document.createElement("span");
		col.className = "author";
		col.innerHTML = GSM_liste[i + start].artiste;
		lign.appendChild(col);
		lign.innerHTML += GSM_liste[i + start].titre
		GSM_addLinkOrder(lign, i + start, tri, type, idAlbum);
		listeC.appendChild(lign);
	    }

	    //On ajuste automatiquement la taille de la police
	    listeC.style.fontSize = Math.min(Math.floor(4000/(GSM_liste.length*1.35))/10, 17) + "px";
	    listeC.style.marginTop = ((410 - listeC.clientHeight) / 2) + "px";
	}
	else
	{
	    var lign = document.createElement("div");
	    lign.className = "elt0";
	    lign.innerHTML = "Il n'y a aucun titre enregistré dans cet album.";
	    $('#bodyList').append(lign);
	}
	lign.className += " foot";
    }
}

function GSM_addLinkCD(elt, idTitle, idAlbum, type, page)
{
    elt.onclick = function() {
	GSM_addForm(type, idAlbum, idTitle+1, page);
    }
}

function GSM_addLinkOrder(elt, id, tri, type, idAlbum)
{
    elt.onclick = function() {
	GSM_chOrder(id, tri, type, idAlbum);
    }
}
function GSM_chOrder(id, tri, type, idAlbum)
{
    var num = prompt("Quel est le numéro de cette chanson : ", id+1);
    if (num > 0 && num <= GSM_liste.length)
    {
	if (num != id)
	{
	    var tmp = GSM_liste[num-1];
	    GSM_liste[num-1] = GSM_liste[id];
	    GSM_liste[id] = tmp;
	    
	    GSM_printliste(tri, type, idAlbum);
	}
    }
    else
	alert("Ce nombre n'est pas valide !\nLe numéro de la chanson doit être compris entre 1 et " + GSM_liste.length);
}

function GSM_apropos()
{
    aproposApp("GSM", "contenu");
}
