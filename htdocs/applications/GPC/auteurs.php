<?php
//Définition de la constante anti-hacking
define("INDEX", 1);

ob_start();
//Inclusion de l'API Onyx
require_once(trim(file_get_contents('../../.onyx')));
require_once(ONYX.'include/functions.php');
$SESS = new Session();
ob_end_clean();

$nom = gpc('auteur', 'post');

$bdd = new BDD();
$bdd->escape($nom);
$chanteurs = $bdd->query("SELECT artiste FROM gsm_titres WHERE artiste LIKE '$nom%' GROUP BY artiste;");
$bdd->deconnexion();

if (!empty($chanteurs) && !empty($nom))
{
	print "<ul>\n";
	foreach ($chanteurs as $chanteur)
	{
		print '    <li>'.$chanteur["artiste"]."</li>\n";
	}
	print "</ul>";
}
?>