﻿var GPC_origin_liste = false;
var GPC_liste = false;
var GPC_liste_CDs = false; //Albums, tout type
var GPC_lastSort = -1;
var GPC_delay = null;
var nbParPage = 50;

//On charge la liste des porte-clés
function GPC_loadList()
{
    GPC_origin_liste = false;
    GPC_liste = false;
    GPC_liste_CDs = false;
    GPC_lastSort = -1;
    $('nbpc').innerHTML = '<img src="applications/GSM/loader.gif" alt="Chargement en cours ...">';
    new Ajax.Request(
 	'ajax.php',
 	{
	    method: 'get',
	    parameters: {d: "action", a: "GPC", p: "liste"},
	    onSuccess: GPC_parseList,
	    onFailure: function() { printEtat(3); }
	}
    );
}
function GPC_loadSelectedList()
{
    //TODO: ne charger que les listes dont l'état est false
    GPC_loadList();
}
GPC_loadList();

//Une fois la liste reçue, on la parse
function GPC_parseList(transport, json)
{
    $('nbpc').innerHTML = json.nombre;

    GPC_origin_liste = new Array();
    var liste = transport.responseXML.documentElement.getElementsByTagName("chanson");
    for (var elt in liste)
    {
	if (elt.isInt())
	{
	    var chan = new Chanson(liste[elt]);
	    GPC_origin_liste[chan.id] = chan;
	}
    }

    GPC_liste_CDs = new Array();
    liste = transport.responseXML.documentElement.getElementsByTagName("album");
    for (var elt in liste)
    {
	if (elt.isInt())
	{
	    var alb = new Album(liste[elt]);
	    GPC_liste_CDs[alb.id] = alb;
	}
    }
}

function GPC_add()
{
    window.scrollTo(0,0);
    $('contenu').innerHTML = "";
    titre = document.createElement("h2");
    titre.innerHTML = "Ajout d'un album à la base de données";
    $('contenu').appendChild(titre);

    GPC_addAlbum();
}

function GPC_edit()
{
    window.scrollTo(0,0);
    $('contenu').innerHTML = "";
    titre = document.createElement("h2");
    titre.innerHTML = "Liste des albums";
    $('contenu').appendChild(titre);

    GPC_viewliste(0, 0, 0);
}

function GPC_listAlbums()
{
    window.scrollTo(0,0);
    $('contenu').innerHTML = "";
    titre = document.createElement("h2");
    titre.innerHTML = "Liste des albums";
    $('contenu').appendChild(titre);

    GPC_viewalbums(false);
}

function GPC_listTitres(types)
{
    window.scrollTo(0,0);
    $('contenu').innerHTML = "";
    titre = document.createElement("h2");
    titre.innerHTML = "Liste des titres";
    $('contenu').appendChild(titre);

    var confirmation = document.createElement("h2");
    confirmation.style.color = "teal";
    confirmation.id = "confirm";
    $('contenu').appendChild(confirmation);

    GPC_liste = false;

    GPC_viewliste(types, 0, 0);
}

function GPC_rechTitres()
{
    window.scrollTo(0,0);
    $('contenu').innerHTML = "";
    titre = document.createElement("h2");
    titre.innerHTML = "Recherche d'un élément";
    $('contenu').appendChild(titre);

    GPC_formRech("", "", "", [1,2,3,4,5,6,7,8]);
}

function GPC_formRech(titleC, artiste, album, types)
{
    var formulaire = document.createElement("form");
    formulaire.id = "rechTitles";
    formulaire.onsubmit = function() {
        tps = [];
        if ($("type1").checked) tps.push(1);
        if ($("type2").checked) tps.push(2);
        if ($("type3").checked) tps.push(3);
        rechTitles($("titreC").value,
                   $("artiste").value,
                   $("album").value,
                   tps);
        return false;
    }
      var cntr = document.createElement("div");
        var lbl = document.createElement("label");
        lbl.htmlFor = "titreC"; lbl.innerHTML = "Titre :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text"; inpt.name = "titre";
        inpt.value = titleC;
        inpt.id = "titreC"; cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));

        var lbl = document.createElement("label");
        lbl.htmlFor = "artiste"; lbl.innerHTML = "Artiste :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text"; inpt.name = "artiste";
        inpt.value = artiste;
        inpt.id = "artiste"; cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));

        var lbl = document.createElement("label");
        lbl.htmlFor = "album"; lbl.innerHTML = "Album :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text"; inpt.name = "album";
        inpt.value = album;
        inpt.id = "album"; cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));
      formulaire.appendChild(cntr);

      var inpt = document.createElement("input");
      inpt.type = "checkbox"; inpt.name = "type1";
      inpt.id = "type1"; inpt.value = "1";
      inpt.checked = types.in_array(1);
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "type1"; lbl.innerHTML = "Albums d'année";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));

      var inpt = document.createElement("input");
      inpt.type = "checkbox"; inpt.name = "type2";
      inpt.id = "type2"; inpt.value = "1";
      inpt.checked = types.in_array(2);
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "type2"; lbl.innerHTML = "Compilations";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));

      var inpt = document.createElement("input");
      inpt.type = "checkbox"; inpt.name = "type3";
      inpt.id = "type3"; inpt.value = "1";
      inpt.checked = types.in_array(3);
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "type3"; lbl.innerHTML = "Musique classique";
      formulaire.appendChild(lbl);

      var cntr = document.createElement("div");
        formulaire.appendChild(document.createElement("br"));
        formulaire.appendChild(document.createElement("br"));
        var inpt = document.createElement("input");
        inpt.type = "submit";
        inpt.value = "Rechercher";
        cntr.appendChild(inpt);
      formulaire.appendChild(cntr);

    $('contenu').appendChild(formulaire);
}

function GPC_addAlbum()
{
    var formulaire = document.createElement("form");
    formulaire.id = "addAlbms";
    formulaire.onsubmit = function() { newAlbum(); return false; }
      var inpt = document.createElement("input");
      inpt.type = "radio";
      inpt.name = "cd";
      inpt.id = "cda";
      inpt.value = "1";
      inpt.onclick = function() { GPC_addAlbum_chColor(1); };
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "cda";
      lbl.innerHTML = "Ajouter un album d'année";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));
      var inpt = document.createElement("input");
      inpt.type = "radio";
      inpt.name = "cd";
      inpt.id = "cdd";
      inpt.value = "1";
      inpt.onclick = function() { GPC_addAlbum_chColor(2); };
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "cdd";
      lbl.innerHTML = "Ajouter d'une compilation";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));
      var inpt = document.createElement("input");
      inpt.type = "radio";
      inpt.name = "cd";
      inpt.id = "cdc";
      inpt.value = "1";
      inpt.onclick = function() { GPC_addAlbum_chColor(3); };
      formulaire.appendChild(inpt);
      var lbl = document.createElement("label");
      lbl.htmlFor = "cdc";
      lbl.innerHTML = "Ajouter un album de musiques classiques";
      formulaire.appendChild(lbl);
      formulaire.appendChild(document.createElement("br"));
      formulaire.appendChild(document.createElement("br"));
      var cntr = document.createElement("div");
        var lbl = document.createElement("label");
        lbl.htmlFor = "nom";
        lbl.innerHTML = "Nom :";
        cntr.appendChild(lbl);
        var inpt = document.createElement("input");
        inpt.type = "text";
        inpt.name = "nom";
        inpt.id = "nom";
        inpt.style.width = "300px";
        cntr.appendChild(inpt);
        cntr.appendChild(document.createElement("br"));
        var lbl = document.createElement("label");
        lbl.htmlFor = "color";
        lbl.innerHTML = "Couleur :";
	cntr.appendChild(lbl);
	var inpt = document.createElement("input");
	inpt.type = "color";
	inpt.name = "color";
	inpt.id = "color";
	inpt.onchange = function() { $("testColor").style.backgroundColor = $('color').value; };
	cntr.appendChild(inpt);
	var test = document.createElement("div");
	test.id = "testColor";
	cntr.appendChild(test);
        cntr.appendChild(document.createElement("br"));
	var lbl = document.createElement("label");
	lbl.htmlFor = "ccf";
	lbl.innerHTML = "Copier la couleur depuis :";
	cntr.appendChild(lbl);
	var inpt = document.createElement("select");
	inpt.name = "ccf";
	inpt.id = "ccf";
	cntr.appendChild(inpt);
	cntr.appendChild(document.createElement("br"));
	cntr.appendChild(document.createElement("br"));
	var inpt = document.createElement("input");
	inpt.type = "submit";
	inpt.value = "Valider";
	cntr.appendChild(inpt);
      formulaire.appendChild(cntr);
    $('contenu').appendChild(formulaire);
}

function GPC_addAlbum_chColor(type)
{
    $("ccf").innerHTML = '<option value="0"> </option>';

    for (var i in GPC_liste_CDs)
    {
	if (i.isInt() && GPC_liste_CDs[i].type == type)
	{
	    var opt = document.createElement("option");
	    opt.value = i;
	    opt.innerHTML = GPC_liste_CDs[i].titre;
	    $("ccf").appendChild(opt);
	}
    }

    if ($("ccf").innerHTML != "")
	$("ccf").onchange = function(event)
                            {
				if (this.value > 0)
				    GPC_addAlbum_loadColor(this, type);
			    };
}

function GPC_addAlbum_loadColor(elt, type)
{
    new Ajax.Request(
 	'ajax.php',
 	{
	    method: 'get',
	    parameters: {d: "action", a: "GSM", p: "color", type: type, id: elt.value},
	    onSuccess: function(transport, json)
	    {
		$('color').value = transport.responseXML.documentElement.getElementsByTagName("color")[0].firstChild.textContent;
		$("testColor").style.backgroundColor = $('color').value;
	    }
	});
}

function newAlbum()
{
    var type;
    if ($('cda').checked == 1)
	type = 1;
    else if ($('cdc').checked == 1)
	type = 3;
    else if ($('cdd').checked == 1)
	type = 2;
    else
    {
	alert("Vous n'avez pas sélectionné de type !");
	return;
    }

    var nom = $('nom').value;
    if (nom)
    {
	//On ajoute l'album
	new Ajax.Request(
	    'ajax.php?d=action&a=GSM&p=add',
	    {
		method: 'post',
		parameters: {
		    type: type,
		    title: nom,
		    color: $('color').value
		},
		onSuccess: function(transport, json)
		{
		    if (json.statut != 1)
		    {
			firstLoad();
			alert("Vous avez été déconnecté. L'album' n'a pas été ajouté.");
		    }
		    else
		    {
			if (transport.responseXML.documentElement.getElementsByTagName("id")[0])
			{
			    GPC_liste_CDs = false;

			    $('contenu').innerHTML = "";
			    titre = document.createElement("h2");
			    titre.style.color = "#FF8800";
			    titre.innerHTML = 'Chargement en cours ... <img src="applications/GSM/loader.gif" alt="Veuilez patienter">';
			    $('contenu').appendChild(titre);

			    //On recharge la liste d'album
			    GPC_loadSelectedList();

			    //On passe à l'écran de modification du nouvel album
			    GPC_viewalbum(type,
					  transport.responseXML.documentElement.getElementsByTagName("id")[0].textContent,
					  0,
					  nom);
			}
			else
			    alert("L'ajout de l'album ne s'est pas passé correctement, vérifiez que cet album n'existe pas déjà.");
		    }
		},
		onFailure: function() { printEtat(3); }
	    }
	);
    }
    else
	alert("Vous n'avez pas précisé de nom pour ce nouvel album.");
}

function GPC_addForm(type, idAlbum, modif, page)
{
    //Si le formulaire existe déjà, on ne fait que remplacer son contenu
    var formulaire;
    if ($('ajout'))
    {
	formulaire = $('ajout');
	formulaire.innerHTML = "";
    }
    else
    {
	formulaire = document.createElement("form");
	formulaire.id = "ajout";
	$('contenu').appendChild(formulaire);
    }

    formulaire.onsubmit = function() { checkAndAdd(type, idAlbum, modif, page); return false; }
    var th = document.createElement("h3");
    if (modif)
	th.innerHTML = "Modification d'un titre";
    else
	th.innerHTML = "Ajout d'un titre à l'album";

    var lab_nom = document.createElement("label");
    lab_nom.innerHTML = "Titre :";
    lab_nom.forHTML = "titrechan";
    var inp_nom = document.createElement("input");
    if(modif)
	inp_nom.value = GPC_liste[modif-1].titre;
    inp_nom.type = "text";
    inp_nom.maxLength = "250";
    inp_nom.id = "titrechan";

    var lab_carac = document.createElement("label");
    lab_carac.innerHTML = "Interpréte :";
    lab_carac.setAttribute("for", "auteur");
    var inp_carac = document.createElement("input");
    if(modif)
	inp_carac.value = GPC_liste[modif-1].artiste;
    inp_carac.type = "text";
    inp_carac.maxLength = "250";
    inp_carac.id = "auteur";

    var auteur_propos = document.createElement("div");
    auteur_propos.id = "auteur_propositions";
    auteur_propos.className = "autocomplete";

    var inp_submit = document.createElement("input");
    inp_submit.type = "submit";
    if (modif)
	inp_submit.value = "Modifier";
    else
	inp_submit.value = "Ajouter";
    inp_submit.style.marginLeft = "165px";

    formulaire.appendChild(th);
    formulaire.appendChild(lab_nom);
    formulaire.appendChild(inp_nom);
    formulaire.appendChild(document.createElement("br"));
    formulaire.appendChild(lab_carac);
    formulaire.appendChild(inp_carac);
    formulaire.appendChild(auteur_propos);
    formulaire.appendChild(document.createElement("br"));
    formulaire.appendChild(document.createElement("br"));
    formulaire.appendChild(inp_submit);
    if (modif)
    {
	var inp_del = document.createElement("input");
	inp_del.type = "button";
	inp_del.value = "Supprimer";
	inp_del.onclick = function() { GPC_del(type, idAlbum, modif, page); };
	inp_del.style.marginLeft = "15px";
	formulaire.appendChild(inp_del);
    }
    if (modif)
	$('titrechan').focus();

    new Ajax.Autocompleter(
        "auteur",
        "auteur_propositions",
        "applications/GSM/auteurs.php",
        {
            paramName: 'auteur',
            minChars: 1
        });
}

function GPC_checkChange(e)
{
    if (this.value == "add")
    {
	if (this.id == "discAuteur")
	    id_name = "new_auteur";
	else if (this.id == "discDece")
	    id_name = "new_decenie";
	else if (this.id == "discAnnee")
	    id_name = "new_annee";

	document.getElementById(id_name).style.display = "inline";
	if (document.getElementById(id_name).value == "")
	    document.getElementById(id_name).value = "Nom du nouvel l'album";
	document.getElementById(id_name).focus();
	document.getElementById(id_name).select();
    }
    else if (this.id == "discAuteur")
	document.getElementById("new_auteur").style.display = "none";
    else if (this.id == "discDece")
	document.getElementById("new_decenie").style.display = "none";
    else if (this.id == "discAnnee")
	document.getElementById("new_annee").style.display = "none";
}

function GPC_del(type, idAlbum, modif, page)
{
    $('titrechan').disabled = true;
    $('auteur').disabled = true;

    var identifiant = GPC_liste[modif-1].id;

    new Ajax.Request(
	'ajax.php?d=action&a=GSM&p=del',
	{
	    method: 'post',
	    parameters: {
		id: identifiant,
		alb: idAlbum
	    },
	    onSuccess: function(transport, json)
	    {
		if (json.statut != 1)
		{
		    firstLoad();
		    alert("Vous avez été déconnecté. La chanson n'a pas été enregistrée");
		}
		else
		{
		    delete GPC_origin_liste[identifiant];

		    setTimeout('$("confirm").innerHTML = "Chanson supprimée avec succès !";', 234);
		    setTimeout("$('confirm').innerHTML = '';", 3500);

		    $('nbpc').innerHTML = json.nombre;

		    if (type)
			GPC_viewalbum(type, idAlbum, page);
		    else
			GPC_viewliste(type, 0, page);

		    if (modif)
			window.scrollTo(0,0);
		}
	    },
	    onFailure: function() { printEtat(3); }
	}
    );

    //On réinitialise les variables de liste pour aller rechercher les listes
    GPC_liste = false;
    GPC_pagination = false;
    $('contenu').innerHTML = "<h2 class=\"vp\" id=\"confirm\">Veuillez patienter suppression de la chanson en cours ...</h2>";
}

function checkAndAdd(type, idAlbum, modif, page)
{
    var modifURI;
    if(modif)
	modifURI = "&id=" + GPC_liste[modif-1].id;
    else
	modifURI = "";

    if ($('titrechan').value == "")
    {
	$('titrechan').className = "erreur";
	alert('Vous n\'avez pas indiqué le titre de la chanson !');
    }
    else if ($('auteur').value == "")
    {
	$('auteur').className = "erreur";
	alert('Vous n\'avez pas indiqué l\'auteur de la chanson !');
    }
    else
    {
	$('titrechan').disabled = true;
	$('auteur').disabled = true;

	new Ajax.Request(
	    'ajax.php?d=action&a=GSM' + modifURI,
	    {
		method: 'post',
		parameters: {
		    titre: $('titrechan').value,
		    auteur: $('auteur').value,
		    type: type,
		    alb: idAlbum
		},
		onSuccess: function(transport, json)
		{
		    if (json.statut != 1)
		    {
			firstLoad();
			alert("Vous avez été déconnecté. La chanson n'a pas été enregistrée");
		    }
		    else
		    {
			if(modif)
			{
			    setTimeout('$("confirm").innerHTML = "Chanson modifiée avec succès !";', 234);
			    setTimeout("$('confirm').innerHTML = '';", 3500);

			    GPC_liste[modif-1].titre = $('titrechan').value;
			    GPC_liste[modif-1].artiste = $('auteur').value;
			}
			else
			{
			    nc = new Chanson();
			    nc.id = transport.responseXML.documentElement.getElementsByTagName("id")[0].textContent;
			    nc.titre = $('titrechan').value;
			    nc.artiste = $('auteur').value;
			    nc.albums.push(idAlbum);
			    GPC_origin_liste[nc.id] = nc;
			    GPC_liste.push(nc);

			    $('titrechan').value = "";
			    $('auteur').value = "";

			    $('titrechan').disabled = false;
			    $('auteur').disabled = false;

			    $('nbpc').innerHTML = json.nombre;

			    setTimeout('$("confirm").innerHTML = "Chanson ajoutée avec succès !";', 234);
			    setTimeout("$('confirm').innerHTML = '';", 3500);
			}

			if (idAlbum)
			    GPC_viewalbum(type, idAlbum, page);
			else
			    //TODO à tester ça marche pas
			    GPC_viewliste(type, 0, page);

			if (modif)
			    window.scrollTo(0,0);
			else
			    $('titrechan').focus();
		    }
		},
		onFailure: function() { printEtat(3); }
	    }
	);
    }
}

function GPC_viewalbums()
{
    if (GPC_origin_liste == false)
	setTimeout(GPC_viewalbums, 200);
    else
    {
	var tableau = document.createElement("div");
	tableau.className = "table cols2 stats";
	tableau.id = "statsA";
	var tableau_head = document.createElement("div");
	tableau_head.className = "head";
	var head_th =  document.createElement("span");
	head_th.innerHTML = "CD années";
	tableau_head.appendChild(head_th);
	var head_th =  document.createElement("span");
	head_th.innerHTML = "Nombre";
	tableau_head.appendChild(head_th);
	tableau.appendChild(tableau_head);
	var tableau_bodyA = document.createElement("div");
	tableau_bodyA.className = "body";
	tableau_bodyA.innerHTML = '<div class="lign elt0 foot">Chargement de la liste ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
	tableau.appendChild(tableau_bodyA);
	$('contenu').appendChild(tableau);

	var tableau = document.createElement("div");
	tableau.className = "table cols2 stats";
	tableau.id = "statsD";
	var tableau_head = document.createElement("div");
	tableau_head.className = "head";
	var head_th =  document.createElement("span");
	head_th.innerHTML = "CD décénies";
	tableau_head.appendChild(head_th);
	var head_th =  document.createElement("span");
	head_th.innerHTML = "Nombre";
	tableau_head.appendChild(head_th);
	tableau.appendChild(tableau_head);
	var tableau_bodyD = document.createElement("div");
	tableau_bodyD.className = "body";
	tableau_bodyD.innerHTML = '<div class="lign elt0 foot">Chargement de la liste ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
	tableau.appendChild(tableau_bodyD);
	$('contenu').appendChild(tableau);

	var tableau = document.createElement("div");
	tableau.className = "table cols2 stats";
	tableau.id = "statsC";
	var tableau_head = document.createElement("div");
	tableau_head.className = "head";
	var head_th =  document.createElement("span");
	head_th.innerHTML = "CD classiques";
	tableau_head.appendChild(head_th);
	var head_th =  document.createElement("span");
	head_th.innerHTML = "Nombre";
	tableau_head.appendChild(head_th);
	tableau.appendChild(tableau_head);
	var tableau_bodyC = document.createElement("div");
	tableau_bodyC.className = "body";
	tableau_bodyC.innerHTML = '<div class="lign elt0 foot">Chargement de la liste ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
	tableau.appendChild(tableau_bodyC);
	$('contenu').appendChild(tableau);


	new Ajax.Request(
 	    'ajax.php',
 	    {
		method: 'get',
		parameters: {d: "action", a: "GSM", p: "stats"},
		onSuccess: function(transport, json)
		{
		    var annees = transport.responseXML.documentElement.getElementsByTagName("annee");
		    var chanteurs = transport.responseXML.documentElement.getElementsByTagName("interprete");
		    var decenies = transport.responseXML.documentElement.getElementsByTagName("decenie");

		    tableau_bodyA.innerHTML = "";
		    for (var i = 0; i < annees.length; i++)
		    {
			if (annees[i].getAttribute("id") != "")
			{
			    var lign = document.createElement("div");
			    lign.className = "elt" + (i%2);
			    GPC_addStatLign(lign, [1], annees[i].getAttribute("id"));
			    var col = document.createElement("span");
			    col.innerHTML = GPC_liste_CDs[annees[i].getAttribute("id")].titre;
			    lign.appendChild(col);
			    var col = document.createElement("span");
			    col.innerHTML = annees[i].getAttribute("nbTitles");
			    lign.appendChild(col);
			    tableau_bodyA.appendChild(lign);
			}
		    }
		    if (annees.length == 0)
			tableau_bodyA.innerHTML = '<div class="lign elt0 foot">Aucun CD</div>';
		    else
			lign.className += " foot";

		    tableau_bodyC.innerHTML = "";
		    for (var i = 0; i < chanteurs.length; i++)
		    {
			if (chanteurs[i].getAttribute("id") != "")
			{
			    var lign = document.createElement("div");
			    lign.className = "elt" + (i%2);
			    GPC_addStatLign(lign, [2], chanteurs[i].getAttribute("id"));
			    var col = document.createElement("span");
			    col.innerHTML = GPC_liste_CDs[chanteurs[i].getAttribute("id")].titre;
			    lign.appendChild(col);
			    var col = document.createElement("span");
			    col.innerHTML = chanteurs[i].getAttribute("nbTitles");
			    lign.appendChild(col);
			    tableau_bodyC.appendChild(lign);
			}
		    }
		    if (chanteurs.length == 0)
			tableau_bodyC.innerHTML = '<div class="lign elt0 foot">Aucun CD</div>';
		    else
			lign.className += " foot";

		    tableau_bodyD.innerHTML = "";
		    for (var i = 0; i < decenies.length; i++)
		    {
			if (decenies[i].getAttribute("id") != "")
			{
			    var lign = document.createElement("div");
			    lign.className = "elt" + (i%2);
			    GPC_addStatLign(lign, [3], decenies[i].getAttribute("id"));
			    var col = document.createElement("span");
			    col.innerHTML = GPC_liste_CDs[decenies[i].getAttribute("id")].titre;
			    lign.appendChild(col);
			    var col = document.createElement("span");
			    col.innerHTML = decenies[i].getAttribute("nbTitles");
			    lign.appendChild(col);
			    tableau_bodyD.appendChild(lign);
			}
		    }
		    if (decenies.length == 0)
			tableau_bodyD.innerHTML = '<div class="lign elt0 foot">Aucun CD</div>';
		    else
			lign.className += " foot";
		},
		onFailure: function() { printEtat(3); }
	    }
	);
    }
}

function GPC_addStatLign(lign, type, id)
{
    lign.onclick = function() { window.scrollTo(0,0); GPC_viewalbum(type, id, 0) };
}

function GPC_viewalbum(type, id, page, title)
{
    window.scrollTo(0,75);
    $('contenu').innerHTML = "";
    var confirmation = document.createElement("h2");
    confirmation.style.color = "teal";
    confirmation.id = "confirm";

    var titre = document.createElement("h2");
    if (typeof(title) == 'undefined')
	titre.innerHTML = "Liste des titres de " + GPC_liste_CDs[id].titre;
    else
	titre.innerHTML = "Liste des titres de " + title;
    $('contenu').appendChild(titre);
    $('contenu').appendChild(confirmation);
    GPC_delay_viewalbum(type, id, page);
}

function GPC_build_liste(id, types)
{
    GPC_liste = new Array();
    for (var i in GPC_origin_liste)
    {
	if (i.isInt() && typeof(GPC_origin_liste[i]) != 'undefined')
	{
	    if (GPC_origin_liste[i].inAlbum(id, types))
		GPC_liste.push(GPC_origin_liste[i]);
	}
    }
}

function rechTitles(title, artiste, album, types)
{
    if (GPC_origin_liste == false)
	GPC_delay = setTimeout(rechTitles, titre, artiste, album, types);
    else
    {
        window.scrollTo(0,0);
        $('contenu').innerHTML = "";
        titre = document.createElement("h2");
        titre.className = "vp";
        titre.innerHTML = "Recherche en cours ...<small>Veuillez patienter</small>";
        $('contenu').appendChild(titre);

        GPC_liste = new Array();
        for (var i in GPC_origin_liste)
        {
	    if (i.isInt() && typeof(GPC_origin_liste[i]) != 'undefined')
	    {
	        if (GPC_origin_liste[i].search(title, artiste, album, types))
		    GPC_liste.push(GPC_origin_liste[i]);
	    }
        }

        $('contenu').innerHTML = "";
        titre = document.createElement("h2");
        titre.innerHTML = "Recherche d'un élément";
        $('contenu').appendChild(titre);

        if (GPC_liste == false)
        {
            titre2 = document.createElement("h3");
            titre2.innerHTML = "Aucun élément n'a été trouvé avec ces critères";
            $('contenu').appendChild(titre2);

            GPC_formRech(title, artiste, album, types);
        }
        else
	    GPC_viewliste(0, 0, 0);
   }
}

function GPC_delay_viewalbum(type, id, page)
{
    if (GPC_origin_liste == false)
	GPC_delay = setTimeout(GPC_delay_viewalbum, 200, type, id, page);
    else
    {
	GPC_build_liste(id);
	if (GPC_liste == false)
	    GPC_liste[0] = 2;

	GPC_lastSort = -1;

	GPC_viewliste(type, id, page);

	GPC_addForm(type, id, false, 0);
    }
}

function GPC_viewliste(type, idAlbum, page)
{
    var pagination = document.createElement("div");
  pagination.className = "pagination top";
    pagination.id = "pagination1";
    $('contenu').appendChild(pagination);

    var tableau = document.createElement("div");
    tableau.className = "table cols3";
    tableau.id = "list";
      var tableau_head = document.createElement("div");
      tableau_head.className = "head";
        var head_th =  document.createElement("span");
        head_th.innerHTML = "Titre";
        head_th.onclick = function() { GPC_showliste("titre", 0, type, idAlbum); }
        tableau_head.appendChild(head_th);
        var head_th =  document.createElement("span");
        head_th.innerHTML = "Chanteur";
        head_th.onclick = function() { GPC_showliste("artiste", 0, type, idAlbum); }
        tableau_head.appendChild(head_th);
        var head_th =  document.createElement("span");
        head_th.innerHTML = "CDs";
        head_th.style.cursor = "auto";
        tableau_head.appendChild(head_th);
      tableau.appendChild(tableau_head);
      var tableau_body = document.createElement("div");
        tableau_body.className = "body";
        tableau_body.id = "bodyList";
        tableau_body.innerHTML = '<div class="lign elt0 foot">Chargement de la liste en cours ... <img src="applications/GSM/loader.gif" alt="Veuillez patienter"></div>';
      tableau.appendChild(tableau_body);
    $('contenu').appendChild(tableau);


    var pagination = document.createElement("div");
    pagination.className = "pagination bottom";
    pagination.id = "pagination2";
    $('contenu').appendChild(pagination);

    GPC_showliste(-1, page, type, idAlbum);
}

function GPC_showliste(tri, page, type, idAlbum)
{
    if (GPC_origin_liste == false)
	setTimeout(GPC_showliste, 200, tri, page, type, idAlbum);
    else
    {
	if (GPC_liste == false)
	    GPC_build_liste(undefined, type);

	//On tente un tri
	if (GPC_lastSort != tri && tri != -1)
	{
	    $('bodyList').innerHTML = '<div class="lign elt0 foot">Tri de la liste en cours ...</div>';
	    GPC_liste = GPC_liste.sort(function(a,b){return a[tri].localeCompare(b[tri]);});
	    GPC_lastSort = tri;
	}

	nbPages = Math.ceil(GPC_liste.length / nbParPage);
	$('pagination1').innerHTML = "";
	$('pagination2').innerHTML = "";

	if (nbPages < 3)
	{
	    var lnkP = document.createElement("a");
	    lnkP.href = "javascript:GPC_printliste('" + GPC_lastSort + "', " + type + ", " + idAlbum + ");";
	    lnkP.innerHTML = "Imprimer la pochette";
	    $('pagination1').appendChild(lnkP);
	    var lnkP = document.createElement("a");
	    lnkP.href = "javascript:GPC_printliste('" + GPC_lastSort + "', " + type + ", " + idAlbum + ");";
	    lnkP.innerHTML = "Imprimer la pochette";
	    $('pagination2').appendChild(lnkP);
	}

	for (var i = 0; i < 4 && i < nbPages; i++)
	    GPC_addLinkPagination(i, type, idAlbum);
	if (page > 5 && i < nbPages)
	{
	    $('pagination1').innerHTML += "...";
	    $('pagination2').innerHTML += "...";
	}
	for (var i = (page < 6?4:page - 2); (i < page + 3 && i < nbPages); i++)
	    GPC_addLinkPagination(i, type, idAlbum);
	if (page <= 5 && i < nbPages)
	{
	    $('pagination1').innerHTML += "...";
	    $('pagination2').innerHTML += "...";
	    for (var i = Math.ceil(nbPages/2) - 2; i < nbPages/2 + 2; i++)
		GPC_addLinkPagination(i, type, idAlbum);
	}
	if (page + 3 < nbPages - 4)
	{
	    $('pagination1').innerHTML += "...";
	    $('pagination2').innerHTML += "...";
	}
	for (var i = (page + 3 >= nbPages - 4?page + 3:nbPages - 4); (i < nbPages); i++)
	    GPC_addLinkPagination(i, type, idAlbum);

	start = page * nbParPage;
	$('bodyList').innerHTML = "";

	if (GPC_liste[0] != 2)
	{
	    for (var i = 0; i < nbParPage && i + start < GPC_liste.length; i++)
	    {
		var lign = document.createElement("div");
		lign.className = "elt" + (i%2);

		GPC_addLinkCD(lign, i + start, idAlbum, type, page);
		var col = document.createElement("span");
		col.innerHTML = GPC_liste[i + start].titre;
		lign.appendChild(col);
		var col = document.createElement("span");
		col.innerHTML = GPC_liste[i + start].artiste;
		lign.appendChild(col);
		var col = document.createElement("span");

		for (var alb in GPC_liste[i + start].albums)
		{
		    if (alb.isInt())
			col.innerHTML += '<a href="javascript:GPC_viewalbum([1,2,3], ' + GPC_liste_CDs[GPC_liste[i + start].albums[alb]].id + ", 0)\">" + GPC_liste_CDs[GPC_liste[i + start].albums[alb]].titre + "</a><br>";
		}
		lign.appendChild(col);
		$('bodyList').appendChild(lign);
	    }
	}
	else
	{
	    var lign = document.createElement("div");
	    lign.className = "elt0";
	    lign.innerHTML = "Il n'y a aucun titre enregistré dans cet album.";
	    $('bodyList').appendChild(lign);
	}
	lign.className += " foot";
    }
}
function GPC_addLinkPagination(page, type, idAlbum)
{
	var lnkP = document.createElement("a");
	lnkP.href = "javascript:GPC_showliste('" + GPC_lastSort + "', " + page + ", " + type + ", " + idAlbum + ");";
	lnkP.innerHTML = page+1;
	$('pagination1').appendChild(lnkP);
	var lnkP = document.createElement("a");
	lnkP.href = "javascript:GPC_showliste('" + GPC_lastSort + "', " + page + ", " + type + ", " + idAlbum + ");";
	lnkP.innerHTML = page+1;
	$('pagination2').appendChild(lnkP);
}

function GPC_printliste(tri, type, idAlbum)
{
    if (GPC_origin_liste == false)
	setTimeout(GPC_printliste, 200, tri, type, idAlbum);
    else
    {
	$('contenu').innerHTML = "";

        var patt1 = "";
        if (GPC_liste_CDs[idAlbum].titre.toLowerCase().indexOf("cd") >= 0)
	    patt1 = GPC_liste_CDs[idAlbum].titre.match(/cd ?([0-9]+)/i).toString();
	var trueTitleL1 = GPC_liste_CDs[idAlbum].titre;
	trueTitleL1 = trueTitleL1.substr(0, trueTitleL1.length - patt1.indexOf(','));
	if (type == 1)
	    trueTitleL1 = "Année " + trueTitleL1;

	var trueTitleL2;
	if (patt1)
	    trueTitleL2 = "CD " + patt1.substr(patt1.indexOf(',')+1);
	else
	    trueTitleL2 = "";

	var trueTitle = trueTitleL1 + " &ndash; " + trueTitleL2;

	var front = document.createElement("div");
	front.className = "front";
	var titleP = document.createElement("div");
	titleP.className = "title";
	titleP.innerHTML = trueTitleL1;
	front.appendChild(titleP);
	var cd = document.createElement("div");
	cd.className = "cd";
	cd.innerHTML = trueTitleL2;
	if (trueTitleL2)
	    cd.style.marginTop = "70px";
	front.appendChild(cd);
	$('contenu').appendChild(front);

	new Ajax.Request(
 	    'ajax.php',
 	    {
		method: 'get',
		parameters: {d: "action", a: "GSM", p: "color", type: type, id: idAlbum},
		onSuccess: function(transport, json)
		{
		    var colors = transport.responseXML.documentElement.getElementsByTagName("color");
		    if (colors.length == 2)
		    {
			titleP.style.color = colors[1].firstChild.textContent;
			cd.style.color = colors[0].firstChild.textContent;
		    }
		    else if (colors.length == 1)
		    {
			titleP.style.color = colors[0].firstChild.textContent;
			cd.style.color = colors[0].firstChild.textContent;
		    }
		}
	    });

	var sp = document.createElement("div");
	sp.className = "sp";
	$('contenu').appendChild(sp);

	var back = document.createElement("div");
	back.className = "back";
	var backL = document.createElement("div");
	backL.className = "left";
	var title = document.createElement("div");
	title.className = "title";
	title.innerHTML = trueTitleL1 + " " + trueTitleL2;
	backL.appendChild(title);
	back.appendChild(backL);
	var backR = document.createElement("div");
	backR.className = "right";
	var title = document.createElement("div");
	title.className = "title";
	title.innerHTML = trueTitleL1 + " " + trueTitleL2;
	backR.appendChild(title);
	back.appendChild(backR);

	var backC = document.createElement("div");
	backC.className = "center";
	var title = document.createElement("div");
	title.className = "title";
	title.innerHTML = trueTitleL1 + " &ndash; " + trueTitleL2;
	backC.appendChild(title);

	var listeC = document.createElement("ol");
	backC.appendChild(listeC);

	back.appendChild(backC);
	$('contenu').appendChild(back);

	if (GPC_liste == false)
	{
	    GPC_liste = new Array();
	    for (var i in GPC_origin_liste)
	    {
		if (i.isInt())
		    GPC_liste.push(GPC_origin_liste[i]);
	    }
	}

	//On tente un tri
	if (GPC_lastSort != tri)
	{
	    GPC_liste = GPC_liste.sort(function(a,b){return a[tri].localeCompare(b[tri]);});
	    GPC_lastSort = tri;
	}

	if (GPC_liste[0] != 2)
	{
	    for (var i = 0; i < GPC_liste.length; i++)
	    {
		var lign = document.createElement("li");
		lign.className = "elt" + (i%2);
		lign.innerHTML = GPC_liste[i + start].titre
		var col = document.createElement("span");
		col.className = "author";
		col.innerHTML = GPC_liste[i + start].artiste;
		lign.appendChild(col);
		GPC_addLinkOrder(lign, i + start, tri, type, idAlbum);
		listeC.appendChild(lign);
	    }

	    //On ajuste automatiquement la taille de la police
	    listeC.style.fontSize = Math.floor(4000/(GPC_liste.length*1.35))/10 + "px";
	    listeC.style.marginTop = ((400 - listeC.clientHeight) / 2) + "px";
	}
	else
	{
	    var lign = document.createElement("div");
	    lign.className = "elt0";
	    lign.innerHTML = "Il n'y a aucun titre enregistré dans cet album.";
	    $('bodyList').appendChild(lign);
	}
	lign.className += " foot";
    }
}

function GPC_addLinkCD(elt, idTitle, idAlbum, type, page)
{
    elt.onclick = function() {
	GPC_addForm(type, idAlbum, idTitle+1, page);
    }
}

function GPC_addLinkOrder(elt, id, tri, type, idAlbum)
{
	elt.onclick = function() {
		GPC_chOrder(id, tri, type, idAlbum);
	}
}
function GPC_chOrder(id, tri, type, idAlbum)
{
	var num = prompt("Quel est le numéro de cette chanson : ", id+1);
	if (num > 0 && num <= GPC_liste.length)
	{
		if (num != id)
		{
			var tmp = GPC_liste[num-1];
			GPC_liste[num-1] = GPC_liste[id];
			GPC_liste[id] = tmp;

			GPC_printliste(tri, type, idAlbum);
		}
	}
	else
		alert("Ce nombre n'est pas valide !\nLe numéro de la chanson doit être compris entre 1 et " + GPC_liste.length);
}

function GPC_apropos()
{
	aproposApp("GPC", "contenu");
}
