var imgTitres_dir = "/images/titres/";
var username = "";
var apps = Array();

String.prototype.isInt = function()
{
    var re = new RegExp("^-?[\\d]+$");
    return this.match(re);
}

Array.prototype.in_array = function(v)
{
    for(var i = 0, l = this.length; i < l; i++)
    {
        if(this[i] == v)
            return true;
    }
    return false;
}

window.onload = function()
{
    printState(1);
    clearScreen();
    firstLoad();
}

function firstLoad()
{
    $.ajax({
	url: "ajax.php",
    }).done(function(data) {
	printState(2);
	if (data.documentElement.getAttribute("statut") == 1)
	{
	    username = data.documentElement.getAttribute("username");

	    if (window.sessionStorage && window.sessionStorage.username == username)
	    {
		//On restaure l'application en cours

	    }
	    else
		page_accueil();
	}
	else
	    loadPage("connexion");
	printState(0);
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

function deconnexion()
{
    printState(1);
    clearScreen();

    $.ajax({
	url: "ajax.php",
	data: {d: "logout"},
    }).done(function(data) {
	firstLoad();
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

function printState(id, appendText)
{
    var state;
    var color = null;
    switch(id)
    {
    case 0:
	state = "";
	break;
    case 1:
	state = "Connexion au serveur en cours ...";
	break;
    case 2:
	state = "Connexion établie, affichage de la page ...";
	color = "green";
	break;
    case 3:
	state = "Impossible de contacter le serveur, délais d'attente expiré !";
	color = "red";
	break;
    case 4:
	state = "Vérification des informations en cours ...";
	color = "orange";
	break;
    case 5:
	state = "Récupération de la liste des applications ...";
	color = "green";
	break;
    case 6:
	state = "Chargement de l'application ...";
	break;
    case 7:
	state = "Une erreur est survenue";
	color = "red";
	break;
    default:
	state = "État inconnu : #" + id;
    };

    if (appendText != null)
	state += ": " + appendText;

    $('#state').html(state);

    if (color)
	$('#state').css("background-color", color);
    else
	$('#state').css("background-color", "#CDF");
}

function clearScreen()
{
    $('#logo').css("display", "block");

    genMenu(username != "");
    $('#corps').html("");

    $('#etatAvance').prop("src", "images/etats/alpha.png");

    //On décharge les fichiers JavaScript et CSS non standards
    $("script").remove(".app");
    $("link").remove(".app");

    //On ajoute le thème par défaut
    if (!$("#main_thm").length)
	$("head").append('<link type="text/css" rel="stylesheet" media="all" id="main_thm" href="css/style.css">');
}

function newMenuItem(text, onclick, link)
{
    item = document.createElement("li");
    itemLink = document.createElement("a");
    if (link)
	itemLink.href = "#" + link;
    else
	itemLink.href = "#";
    itemLink.onclick = onclick;
    itemLink.innerHTML = text;
    item.appendChild(itemLink);
    return item;
}

function genMenu(online)
{
    //On supprime tous les éléments du menu
    $('#menu').empty();

    //On ajoute le titre
    $('#menu').append('<li class="nameApp">PA4home</li>');

    //Lien vers la page d'accueil
    $('#menu').append(newMenuItem("Accueil", firstLoad));

    if (online)
	$('#menu').append(newMenuItem("Déconnexion", deconnexion));

    $('#menu').append('<li class="divider"></li>');

    //Lien vers la page à propos du site
    $('#menu').append(newMenuItem("À propos du site",
				  function() { loadPage('aproposdusite'); },
				  "about-PA4home"));
}

function addMenuItem(text, iven)
{
    $('#menu').append(newMenuItem(text,
				  function() { eval(iven) })
		     );
}

/**************************************
 * Partie contenant les pages du site *
 **************************************/

function loadPage(nom)
{
    clearScreen();
    printState(1);

    $.ajax({
	url: "ajax.php",
	data: { d: "page", p: nom }
    }).done(function(data) {
	printState(2);

	$('#titre').prop("src", imgTitres_dir + data.documentElement.getElementsByTagName("titre")[0].firstChild.data);
	$('#corps').html(data.documentElement.getElementsByTagName("body")[0].firstChild.data);

	if (data.documentElement.getElementsByTagName("js"))
	{
	    var scripts = data.documentElement.getElementsByTagName("js");
	    var nbScripts = scripts.length;
	    for (i = 0; i < nbScripts; i++)
		eval(scripts[i].firstChild.data);
	}

	printState(0);
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

function page_accueil()
{
    clearScreen();
    printState(5);

    if (username == "root" || username == "pierre-o")
	$('#titre').prop("src", imgTitres_dir + "bienvenuePierreO.png");
    else if (username == "sergcaen" || username == "serge")
	$('#titre').prop("src", imgTitres_dir + "bienvenueSerge.png");
    else if (username == "christine" || username == "chriscaen")
	$('#titre').prop("src", imgTitres_dir + "bienvenueChristine.png");
    else if (username == "raphael" || username == "minou")
	$('#titre').prop("src", imgTitres_dir + "bienvenueRaphael.png");
    else if (username == "florence" || username == "floflo")
	$('#titre').prop("src", imgTitres_dir + "bienvenueFlorence.png");
    else if (username == "florent")
	$('#titre').prop("src", imgTitres_dir + "bienvenueFlorent.png");
    else if (username == "alisson13081991" || username == "alisson13081991@hotmail.fr")
	$('#titre').prop("src", imgTitres_dir + "bienvenueAlisson.png");
    else
	$('#titre').prop("src", imgTitres_dir + "bienvenue.png");

    $.ajax({
	url: "ajax.php",
	data: {d: "accueil"},
    }).done(function(data) {
	printState(2);
	var applications = data.documentElement.getElementsByTagName("application");
	for (i = applications.length - 1; i >= 0; i--)
	{
	    var app = new Application(applications[i]);
	    apps.pop(app);

	    var elt = app.getHomeElt();

	    $('#corps').append(elt);
	}
	printState(0);
    }).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

function runApplication(property, display, json){
    alert("Aucun lancement à faire !");
}

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
	return entityMap[s];
    });
}

//Menu
var menu_timeout;

function showMenu()
{
    if (menu_timeout)
	clearTimeout(menu_timeout);

    $('menu').style.display = "block";
}

function setHideMenuTimeout()
{
    menu_timeout = setTimeout('hideShownMenu()', 500);
}

function hideShownMenu()
{
    $('menu').style.display = "none";
}
