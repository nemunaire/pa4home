var URL_SERVEUR = 'http://hsc.escrime.local/'; //URL du serveur
var last_position_bandeau = true; //Dernière position du bandeau définie par l'utilisateur
var fade_objects = {loading: {statut: -1, timer: false}, secondBody: {statut: -1, timer: false}};
var ajax_save_html = false;
var ajax_save_json = false;
var ajax_time_sauve = false;

function loadPage(url)
	{
		$('secondBody').className = document.getElementsByTagName("body").item(0).className;
		decomp = url.split("?");
		start_fadeIn('loading');
		start_fadeIn('secondBody');
		if (decomp.length >= 2) {
			new Ajax.Request(
			 'ajax.php',
			 {
				method: 'post',
				parameters: {url: decomp[1]},
				onSuccess: onSuccess_ajax,
				onFailure: function() { loadPage("ici?p=accueil"); alert('Une erreur s\'est produite lors du téléchargement de la page !'); }
			 }
			);
			hideShownMenu();
			return false;
		}

		ajax_time_sauve = setTimeout("rescue", 15000);
	}

function onSuccess_ajax(transport, json)
	{
		ajax_save_html = transport.responseText;
		if (json != null)
			ajax_save_json = json;
		else
			ajax_save_json = false
		printPage();
	}

function printPage()
	{
		if (fade_objects["secondBody"]["statut"] != 1)
			setTimeout("printPage()", 500);
		else
			{
				$('contenu').innerHTML = ajax_save_html;
				verifLinks();
				if (ajax_save_json != false)
					{
						document.getElementsByTagName("body").item(0).className = ajax_save_json.class;
						if (ajax_save_json.bandeau && (document.getElementById('submenuContainer').style.top == "221px" || document.getElementById('submenuContainer').style.top == "220px"))
							ajax_hideBandeau();
						else if (document.getElementById('submenuContainer').style.top == "51px" && last_position_bandeau && !ajax_save_json.bandeau)
							ajax_showBandeau();

						if (ajax_save_json.special == "resultats")
							load_pageResultats();
						else if (ajax_save_json.special == "maps")
							{
    							var map = new GMap2(document.getElementById('map'));
								initMap();
							}
					}

				start_fadeOut('loading');
				start_fadeOut('secondBody');

			}
	}

function verifLinks()
	{
		totalLiens = document.getElementsByTagName("a");
		for (var i=0 ; i < totalLiens.length ; i++)
			{
			//TODO tester si le lien appartient au serveur, s'il n'y appartient pas, on laisse le lien en normal
				if (!document.getElementsByTagName("a").item(i).onclick) {
					document.getElementsByTagName("a").item(i).onclick = function onclick(event)
						{
							loadPage(this.href);
							return false;
						};
				}
			}
	}

function ajax_hideBandeau()
	{
		ajax_rec_hideBandeau(0);
	}

function ajax_rec_hideBandeau(pos)
	{
		pos = pos + 15;
		if (pos > 169) pos = 170;
		modifyBandeau(pos);
		if (pos <= 169) setTimeout('ajax_rec_hideBandeau(' + pos + ')', 0);
	}


function ajax_showBandeau()
	{
		ajax_rec_showBandeau(170);
	}

function ajax_rec_showBandeau(pos)
	{
		pos = pos - 15;
		if (pos < 0) pos = 0;
		modifyBandeau(pos);
		if (pos > 0) setTimeout('ajax_rec_showBandeau(' + pos + ')', 0);
	}

function load_pageResultats()
	{
		initAutoCompleter();
		$('formResultats').onsubmit = function onsubmit(event)
			{
				$('secondBody').className = document.getElementsByTagName("body").item(0).className;
				start_fadeIn('loading');
				start_fadeIn('secondBody');
				new Ajax.Request(
			 'ajax.php',
			 {
				method: 'post',
				parameters: {url: "p=resultats", nom: $('nomtireur').value},
				onSuccess: onSuccess_ajax,
				onFailure: function() { alert('Failure !'); }
			 }
			);
			return false;
			};
	}

function start_fadeIn(id)
	{
		if (fade_objects[id]["statut"] == 0)
			fade_objects[id]["timer"] = setTimeout("start_fadeIn(" + id + ")", 500);
		else if (fade_objects[id]["statut"] != 1)
			{
				changeOpac(0, id);
				document.getElementById(id).style.display = "block";
				fade_objects[id]["statut"] = 0;

				fadeIn(0, id);
			}
	}

function start_fadeOut(id)
	{
		if (fade_objects[id]["statut"] == 0)
			fade_objects[id]["timer"] = setTimeout("start_fadeOut(" + id + ")", 500);
		else if (fade_objects[id]["statut"] != -1)
			{
				changeOpac(100, id);
				document.getElementById(id).style.display = "block";
				fade_objects[id]["statut"] = 0;

				fadeOut(10, id);
			}
	}

function fadeIn(i, id)
	{
		i++;
		changeOpac(i*10, id);

		if (i < 10)
			setTimeout("fadeIn(" + i + ", '" + id + "')", 30);
		else
			fade_objects[id]["statut"] = 1;
	}

function fadeOut(i, id)
	{
		i--;
		changeOpac(i*10, id);

		if (i > 0)
			setTimeout("fadeOut(" + i + ", '" + id + "')", 30);
		else
			{
				document.getElementById(id).style.display = "none";
				fade_objects[id]["statut"] = -1;
			}
	}

function changeOpac(opacity, id) {
	if (opacity > 100) opacity = 100;
    var object = document.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
}