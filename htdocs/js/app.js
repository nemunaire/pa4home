function Application(XMLproperties)
{
    this.name = XMLproperties.getElementsByTagName("name")[0].firstChild.data;
    this.dir = XMLproperties.getElementsByTagName("dir")[0].firstChild.data;

    if (XMLproperties.getElementsByTagName("description")[0])
		this.description = XMLproperties.getElementsByTagName("description")[0].firstChild.data;
    else
		this.description = null;

    this.etatAvancement = XMLproperties.getElementsByTagName("etatAvancement")[0].firstChild.data;
    this.version = XMLproperties.getElementsByTagName("version")[0].firstChild.data;

    if (XMLproperties.getElementsByTagName("lang")[0])
		this.lang = XMLproperties.getElementsByTagName("lang")[0].firstChild.data;
    else
		this.lang = null;

    if (XMLproperties.getElementsByTagName("developpeur")[0])
		this.developpeur = XMLproperties.getElementsByTagName("developpeur")[0].firstChild.data;
    else
		this.developpeur = null;

    if (XMLproperties.getElementsByTagName("corps")[0])
		this.corps = XMLproperties.getElementsByTagName("corps")[0].firstChild.data;
    else
		this.corps = "corps";

    this.menu = null;
}

Application.prototype.getHomeElt = function()
{
    var app = document.createElement("dl");
    app.className = "application " + this.etatAvancement;

    var appTitle = document.createElement("dt");
    appTitle.innerHTML = this.name;
    var appDescription = document.createElement("dd");
    appDescription.innerHTML = this.description;

    $(app).bind('click', this.load.bind(this));

    app.appendChild(appTitle);
    app.appendChild(appDescription);

    return app;
}

Application.prototype.load = function()
{
    printState(6);

    $.ajax({
	url: "ajax.php",
	data: {d: "display", a: this.dir},
    }).done(this.loaded.bind(this)).fail(function(jqXHR, textStatus) {
	printState(7, textStatus);
    });
}

Application.prototype.loaded = function(data)
{
    clearScreen();

    var display = data.documentElement.getElementsByTagName("display")[0];
    var property = data.documentElement.getElementsByTagName("property")[0];

    if (display.getElementsByTagName("titre")[0])
	$('#titre').prop("src", imgTitres_dir + display.getElementsByTagName("titre")[0].firstChild.data);

    if (display.getElementsByTagName("css"))
    {
	//On supprime le style par défaut
	$("#main_thm").remove();

	var styles = display.getElementsByTagName("css");
	for (i = styles.length - 1; i >= 0; i--)
	{
	    var css_file = document.createElement("link");
	    css_file.type = 'text/css';
	    css_file.rel = 'stylesheet';
	    css_file.className = 'app';
	    if (styles[i].getAttribute("media"))
		css_file.media = styles[i].getAttribute("media");
	    css_file.href = "applications/" + this.dir + "/"  + styles[i].firstChild.data;

	    document.getElementsByTagName('head')[0].appendChild(css_file);
	}
    }

    if (display.getElementsByTagName("script"))
    {
	var scripts = display.getElementsByTagName("script");
	for (i = scripts.length - 1; i >= 0; i--)
	{
	    js_file = document.createElement("script");
	    js_file.type = 'text/javascript';
	    js_file.className = 'app';
	    js_file.src = "applications/" + this.dir + "/"  + scripts[i].firstChild.data;
	    js_file.defer = true;

	    document.getElementsByTagName('head')[0].appendChild(js_file);
	}
    }

    if (display.getElementsByTagName("background").length > 0)
	$('#body').css("backgroundImage", "url(" + display.getElementsByTagName("background")[0].firstChild.data + ")");

    var etat;
    if (property.getElementsByTagName("etatAvancement").length > 0)
	etat = property.getElementsByTagName("etatAvancement")[0].firstChild.data;
    else
	etat = "";

    if (etat == "En construction" || etat == "en construction" || etat == "enconstruction" || etat == "construction" || etat == "c")
	$('#etatAvance').prop("src", "images/etats/construction.png");
    else if (etat == "Alpha" || etat == "alpha" || etat == "a")
	$('#etatAvance').prop("src", "images/etats/alpha.png");
    else if (etat == "Beta" || etat == "beta" || etat == "Béta" || etat == "béta" || etat == "b")
	$('#etatAvance').prop("src", "images/etats/beta.png");
    else
	$('#etatAvance').prop("src", "images/etats/final.png");

    //On affiche la page
    if (display.getElementsByTagName('body')[0])
	$('#corps').html(display.getElementsByTagName('body')[0].firstChild.data);

    //On prépare le menu
    if (property.getElementsByTagName("menu").length > 0)
	this.menu = eval(property.getElementsByTagName("menu")[0].firstChild.data);
    this.genMenu(username != "");

    //On lance l'application
    if (display.getElementsByTagName("js")[0])
    {
	var scripts = display.getElementsByTagName("js");
	for (i = scripts.length - 1; i >= 0; i--)
	    eval(scripts[i].firstChild.data);

	runApplication(property, display);
    }
    else
	setTimeout("runApplication" + this.dir + "(" + property + ", " + display + ");", 1);

    printState(0);
}

Application.prototype.genMenu = function(online)
{
    //On supprime tous les éléments du menu actuel
    $('#menu').empty();

    //On ajoute le titre
    $('#menu').append('<li class="nameApp">' + this.name + "</li>");

    //Lien vers la page d'accueil
    $('#menu').append(newMenuItem("Accueil", firstLoad));

    //Lien de déconnexion
    if (online)
	$('#menu').append(newMenuItem("Déconnexion", deconnexion));

    $('#menu').append('<li class="divider"></li>');

    //On insère les éléments de l'app
    if (this.menu)
    {
	for (var i = this.menu.length - 1; i >= 0; i--)
	    addMenuItem(this.menu[i]["text"], this.menu[i]["eventClick"]);
    }

    //On insère le about app
    $('#menu').append(newMenuItem("À propos de ...",
				  function() { this.about.bind(this); },
				  "about-app"));

    $('#menu').append('<li class="divider"></li>');

    //Lien vers la page à propos du site
    $('#menu').append(newMenuItem("À propos du site",
				  function() { loadPage('aproposdusite'); },
				  "about-PA4home"));
}

Application.prototype.about = function()
{
    hideShownMenu();
    $(this.corps).innerHTML = "";

    //Texte correspondant à l'état de l'app
    var txtEtat = "";
    if (this.etatAvancement == "En construction" || this.etatAvancement == "en construction" || this.etatAvancement == "enconstruction" || this.etatAvancement == "construction" || this.etatAvancement == "c")
		txtEtat = "En construction";
    else if (this.etatAvancement == "Béta" || this.etatAvancement == "Beta" || this.etatAvancement == "beta" || this.etatAvancement == "béta" || this.etatAvancement == "b")
		txtEtat = "Béta";
    else if (this.etatAvancement == "Alpha" || this.etatAvancement == "alpha" || this.etatAvancement == "a")
		txtEtat = "Alpha";

    var titre = document.createElement("h2");
    titre.innerHTML = "A propos de " + this.name + " pour Pommultimedia for Home";
    $(this.corps).appendChild(titre);
    titre = document.createElement("h3");
    titre.innerHTML = this.name + "<br />Version " + this.version + " " + txtEtat;
    $(this.corps).appendChild(titre);
    if (this.description != null)
    {
		titre = document.createElement("h4");
		titre.innerHTML = "<ins>Description :</ins> " + this.description;
		$(this.corps).appendChild(titre);
    }
    if (this.lang != null)
    {
		titre = document.createElement("h4");
		titre.innerHTML = "<ins>Langue :</ins> " + this.lang;
		$(this.corps).appendChild(titre);
    }
    titre = document.createElement("h4");
    titre.innerHTML = "<ins>Nom court :</ins> " + this.dir;
    $(this.corps).appendChild(titre);
    if (this.developpeur != null)
    {
		titre = document.createElement("h4");
		titre.innerHTML = "<ins>Développeur :</ins> " + this.developpeur;
		$(this.corps).appendChild(titre);
    }
}
