<?php
if (!defined("ONYX"))
	exit;

define("APPSDIR", ONYX."include/applications/");
define("PAGESDIR", ONYX."include/pages/");

function acces_application($application, $applicationXML = null)
{
	global $SESS;

	$autoriser = false;

	if (empty($applicationXML))
	{
		$applicationXML = new DOMDocument();
		$applicationXML->load(APPSDIR.$application.'/property.xml');
	}

	if ($applicationXML->documentElement->getAttribute('active') && ($applicationXML->getElementsByTagName('application') || $applicationXML->getElementsByTagName('page')))
	{
		foreach($applicationXML->getElementsByTagName('security') as $security)
		{
			//Autorisation par défaut
			if ($security->getAttribute('default') == "connected")
				$autoriser = !empty($SESS->values["connecte"]);
			elseif ($security->getAttribute('default') == "authorized")
				$autoriser = true;

			//Méthode(s) d'autorisation(s)
			if (ereg("users", $security->getAttribute('type')) && !empty($SESS->values["username"]))
			{
				foreach($applicationXML->getElementsByTagName('user') as $user)
				{
					if (strtolower($user->textContent) == strtolower($SESS->values["id_user"]))
						return !$autoriser;
				}
			}
			if (ereg("usernames", $security->getAttribute('type')) && !empty($SESS->values["username"]))
			{
				foreach($applicationXML->getElementsByTagName('username') as $user)
				{
					if (strtolower($user->textContent) == strtolower($SESS->values["username"]))
						return !$autoriser;
				}
			}
			if (ereg("ips", $security->getAttribute('type')))
			{
				foreach($applicationXML->getElementsByTagName('ip') as $ip)
				{
					if ($ip->textContent == $_SERVER["REMOTE_ADDR"])
						return !$autoriser;
				}
			}
		}
	}
	return $autoriser;
}

function send404(&$xml_root)
{
	global $xml;

	$xml_root->appendChild($xml->createElement("titre", "404.png"));
	$xml_root->appendChild($xml->createElement("body", "Page introuvable"));
}

function send403(&$xml_root)
{
	global $xml;

	$xml_root->appendChild($xml->createElement("titre", "avertissements.png"));
	$xml_root->appendChild($xml->createElement("body", "<h2>Erreur 403</h2><h3>Vous n'êtes pas autorisé à accéder à cette page.</h3>Si vous êtes sur d'avoir le droit d'accèder à cette page et que le problème perciste, contacter l'administrateur."));
}

if (!function_exists("json_encode"))
{
	include_once(ONYX.'include/JSON.php');
	function json_encode($value, $option = 0)
	{
		$json = new Services_JSON();
		return $json->encode($value);
	}
}
?>