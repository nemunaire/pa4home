<?php
//Fichier appelé pour afficher l'application
if (isset($_POST['message']))
{
	$message = strip_tags(trim(gpc("message", "post")));
	if (!empty($message) || $message == "0")
	{
		$bdd = new BDD();
		$bdd->escape($message);
		$bdd->query("INSERT INTO chat (id_membre, timestamp, message) VALUES (".$SESS->values['id_user'].", ".time().", '$message');");
		$bdd->deconnexion();
	}
}
elseif (isset($_POST['comm']))
{
	$commande = strip_tags(trim(gpc("comm", "post")));
	if ($commande == "!clear")
	{
		if ($SESS->values["id_user"] == 1)
		{
			$bdd = new BDD();
			$bdd->query("DELETE FROM chat;");
			$bdd->deconnexion();
			$json["confirm"] = "Tous les messages ont été supprimés";
		}
		else
			$json["confirm"] = "Vous n'avez pas les autorisations nécessaires pour effacer les messages.";
	}
	elseif ($commande == "!online")
	{
		$users = Cache::read('chat_online');
		$json["confirm"] = "Actuellement en ligne :<ul>";
		foreach($users as $username => $time)
		{
			if (time()-30 < $time)
				$json["confirm"] .= "<li>$username</li>";
		}
		$json["confirm"] .= "</ul>";
	}
	elseif ($commande == "!help" || $commande == "!hlp" || $commande == "!aide")
	{
		$json["confirm"] = "<table><tr><th>Commande</th><th>Description</th></tr>
			<tr><td>!changeRefreshTime <ins>int</ins></td><td>Change le temps de rafraîchissement à <ins>int</ins> (milli)secondes</td></tr>
			<tr><td>!cls</td><td>Nettoie l'écran</td></tr>
			<tr><td>!clear</td><td>Supprime tous les messages enregistrés dans la base de données</td></tr>
			<tr><td>!help</td><td>Affiche cet aide</td></tr>
			<tr><td>!maj</td><td>Actualise les messages du chat</td></tr>
			<tr><td>!online</td><td>Montre les utilisateurs en ligne (ces 30 dernières secondes)</td></tr>
			<tr><td>!quit</td><td>Quitte le chat et affiche l'accueil</td></tr>
			<tr><td>!reset</td><td>Réaffiche les 15 derniers messages envoyés</td></tr>
			</table>";
	}
}
else
{
	//Met à jour la liste des personnes en ligne
	$users = Cache::read('chat_online');
	$users[$SESS->values["username"]] = time();
	Cache::set('chat_online', $users);

	if (!empty($_GET['time']))
		$time = intval(gpc("time"))." ORDER BY timestamp DESC";
	else
		$time = "0 ORDER BY timestamp DESC LIMIT 15";

	$bdd = new BDD();
	$messages = $bdd->query("SELECT C.*, U.pseudo FROM chat C INNER JOIN users U ON U.id = C.id_membre WHERE C.timestamp > $time;");
	$bdd->deconnexion();

	$json["messages"] = $messages;
}
?>