<?php
//Définition de la constante anti-hacking
define("INDEX", 1);

require_once(trim(file_get_contents('./.onyx')));
require_once(ONYX.'include/functions.php');

$bdd = new BDD();
/*
//$res = $bdd->query("SELECT * FROM gsm_cdannee;");
$res = $bdd->query("SELECT * FROM gsm_cddece;");
//$res = $bdd->query("SELECT * FROM gsm_cdannee;");
foreach($res as $cd)
{
  $type = 2;
  $nom = $cd["nom"];
  $color = $cd["color"];
  $bdd->escape($nom);
  $bdd->query("INSERT INTO gsm_albums (type, titre, color) VALUES ($type, '$nom', $color);");
}
*/

session_start();
$_SESSION["page"]++;

$start = $_SESSION["page"] * 500;

$res = $bdd->query("SELECT * FROM gsm LIMIT $start, 500;");
foreach($res as $cd)
{
  // Insert the title
  $titre = $cd["titre"];
  $chanteur = $cd["chanteur"];
  $bdd->escape($titre);
  $bdd->escape($chanteur);
  $bdd->query("INSERT INTO gsm_titres (titre, artiste) VALUES ('$titre', '$chanteur');");
  $idtitre = $bdd->insert_id();

  if (empty($idtitre))
  {
    $r = $bdd->unique_query("SELECT id FROM gsm_titres WHERE titre = '$titre' AND artiste = '$chanteur';");
    if (empty($r) || empty($r["id"]))
      die("empty ID on ".$res["id"]);
    $idtitre = $r["id"];
  }

  // Found CD
  if (!empty($cd["CDDeca"]))
  {
    $deca = $bdd->unique_query("SELECT nom, color FROM gsm_cddece WHERE id = ".$cd["CDDeca"]);
    $album = $bdd->unique_query("SELECT id FROM gsm_albums WHERE type = 2 AND titre = '".$deca["nom"]."' AND color = ".$deca["color"]);
    if (isset($album))
      $bdd->query("INSERT INTO gsm_in VALUES ($idtitre, ".$album["id"].");");
  }
  if (!empty($cd["CDAnnee"]))
  {
    $deca = $bdd->unique_query("SELECT nom, color FROM gsm_cdannee WHERE id = ".$cd["CDAnnee"]);
    $album = $bdd->unique_query("SELECT id FROM gsm_albums WHERE type = 1 AND titre = '".$deca["nom"]."' AND color = ".$deca["color"]);
    if (isset($album))
      $bdd->query("INSERT INTO gsm_in VALUES ($idtitre, ".$album["id"].");");
  }
}


$bdd->deconnexion();
?>