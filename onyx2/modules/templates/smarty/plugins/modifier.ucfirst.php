<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty capitalize modifier plugin
 *
 * Type:     modifier<br>
 * Name:     capitalize<br>
 * Purpose:  capitalize words in the string
 * @link http://smarty.php.net/manual/en/language.modifiers.php#LANGUAGE.MODIFIER.CAPITALIZE
 *      capitalize (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */
function smarty_modifier_ucfirst($chaine)
{
    $chaineS = strtr($chaine, �äâàáåãéèëêòóôõöøìíîïùúûüýñçþÿæ�ðø�,�ÄÂÀÁÅÃÉÈËÊÒÓÔÕÖØÌÍÎÏÙÚÛÜÝÑÇÞÝÆ�ÐØ�);
    if ($chaineS[0].$chaineS[1] != $chaine[0].$chaine[1])
        return $chaineS[0].$chaineS[1].substr($chaine, 2);
    else
        return ucfirst($chaine);
}


?>
