<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty separe number modifier plugin
 *
 * Type:     modifier<br>
 * Name:     separenombre<br>
 * Date:     Aug 22, 2008
 * Purpose:  separe number
 * Example:  {$int|separenombre}
 *
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param float $
 * @return string
 */
function smarty_modifier_separerNombres($int)
{
    return number_format(floor($int), 0, ',', ' ');
}

?>