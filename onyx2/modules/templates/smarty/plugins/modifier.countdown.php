<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty countdown modifier plugin
 *
 * Type:     modifier<br>
 * Name:     countdown<br>
 * Date:     Apr 15, 2009
 * Example:  {$seconds|countdown}
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param timestamp
 * @return string
 */
function smarty_modifier_countdown($secondes)
{
    $heures = intval($secondes/3600);
    if ($heures < 10) $heures = '0'.$heures;
    $minutes = intval(($secondes%3600)/60);
    if ($minutes < 10) $minutes = '0'.$minutes;
    $secondes = $secondes%60;
	if ($secondes < 10) $secondes = '0'.$secondes;

    return $heures.':'.$minutes.':'.$secondes;
}

?>