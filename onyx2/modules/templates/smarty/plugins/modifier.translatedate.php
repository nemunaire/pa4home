<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_translatedate($string)
{
	$Mfr = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
	$Men = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	$Jfr = array("lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche");
	$Jen = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
	return str_replace($Jfr, $Jen, str_replace($Mfr, $Men, $string));
}

/* vim: set expandtab: */

?>