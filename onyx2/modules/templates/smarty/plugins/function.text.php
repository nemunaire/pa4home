<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {text} function plugin
 *
 * Type:     function<br>
 * Name:     text<br>
 * @author Némunaire <nemunaire@gmail.com>
 */
function smarty_function_text($params, &$template)
{
  if (empty($params["lang"]))
    {
      if (isset($params['assign']))
	$template->assign($params['assign'], Lang::getText($params["file"], $params["path"]));
      else
	return Lang::getText($params["file"], $params["path"]);
    }
  else
    {
      if (isset($params['assign']))
	$template->assign($params['assign'], Lang::getText($params["file"], $params["path"], $params["lang"]));
      else
	return Lang::getText($params["file"], $params["path"], $params["lang"]);
    }
}

?>