<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty calc temps modifier plugin
 *
 * Type:     modifier<br>
 * Name:     temps<br>
 * Date:     Nov 01, 2009
 * Purpose:  timestamp to string
 * Example:  {$timestamp|separenombre}
 *
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param timestamp $
 * @return string
 */
function smarty_modifier_temps($time)
{
	$output = '';
	$tab = array ('jour' => '86400', 'heure' => '3600', 'minute' => '60', 'seconde' => '1');
	foreach ($tab as $key => $value) {
		$compteur = 0;
		while ($time > ($value-1)) {
			$time = $time - $value;
			$compteur++;
		}
		if ($compteur != 0) {
			$output .= $compteur.' '.$key;
			if ($compteur > 1) $output .= 's';
			if ($value != 1) $output .= ' ';
		}
	}
	if (empty($output)) return 'Instantané';
	else return $output;
}

/* vim: set expandtab: */

?>