<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier
 * Name:     status
 * Date:     Feb 2, 2011
 * Purpose:  print the text corresponding to the state
 * Example:  {$int|status}
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param int
 * @return string
 */
function smarty_modifier_status($int, $bool = false)
{
  if ($bool)
    {
      if ($int < 1)
	{
	  if ($int > -2)
	    {
	      if ($int == 0)
		return "Nouveau document !";
	      else //$int == -1
		return "Document en cours de création/correction";
	    }
	  else
	    {
	      if ($int == -2)
		return "Refusé";
	      else //$int == -3
		return "Supprimé";
	    }
	}
      else
	{
	  if ($int > 3)
	    {
	      if ($int == 4)
		return "Publié";
	      else //$int == 5 || $int == 6
		return "Mis en valeur";
	    }
	  else
	    {
	      if ($int == 2)
		return "Corrigé";
	      elseif ($int == 1)
		return "En cours de correction";
	      else //$int == 3
		return "Contrôle";
	    }
	}
    }
  else
    {
      if ($int < 1)
	{
	  if ($int > -2)
	    {
	      if ($int == 0)
		return "Document en attente de correction";
	      else //$int == -1
		return "Document à compléter";
	    }
	  else
	    {
	      if ($int == -2)
		return "Document refusé";
	      else //$int == -3
		return "Document supprimé";
	    }
	}
      else
	{
	  if ($int > 3)
	    {
	      if ($int == 4)
		return "Document publié";
	      else //$int == 5 || $int == 6
		return "Document mis en valeur";
	    }
	  else
	    {
	      if ($int == 2)
		return "Corrigé";
	      elseif ($int == 1)
		return "En cours de correction";
	      else //$int == 3
		return "En cours de contrôle";
	    }
	}
    }
}

/* vim: set expandtab: */

?>