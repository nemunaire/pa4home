<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier
 * Name:     gravatar
 * Date:     Feb 2, 2011
 * Purpose:  print the url to the image for gravatar
 * Example:  {$mail|gravatar}
 * @version  1.0
 * @author   Nemunaire <nemunaire at gmail dot com>
 * @param int
 * @return string
 */
function smarty_modifier_gravatar($mail, $comp = "")
{
  return "http://www.gravatar.com/avatar/".md5(strtolower(trim($mail))).$comp;
}

/* vim: set expandtab: */

?>