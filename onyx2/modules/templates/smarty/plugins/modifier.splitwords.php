<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */

function smarty_modifier_splitwords($string, $max = 77)
{
  $words = preg_split('/\s/', $string);
  $lines = array();
  $line = '';
  
  foreach ($words as $k => $word)
    {
      $length = strlen($line . ' ' . $word);
      if ($length <= $max)
	$line .= ' ' . $word;
      else if ($length > $max)
	{
	  if (!empty($line))
	    $lines[] = trim($line);
	  $line = $word;
	}
      else
	{
	  $lines[] = trim($line) . ' ' . $word;
	  $line = '';
	}
    }
  $lines[] = ($line = trim($line)) ? $line : $word;
  
  if (!empty($lines[1]))
    return $lines[0]." ...";
  else
    return $lines[0];
}
?>