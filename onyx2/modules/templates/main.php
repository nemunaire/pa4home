<?php

if(!defined('ONYX')) exit;

define('SMARTY_DIR',ONYX.'modules/templates/smarty/');

require_once(SMARTY_DIR."Smarty.class.php");

define('_TEMPLATE_DIR',ONYX.'tpl/'.$OPT['tpl'].'/');
define('_TEMPLATE_COMPILE',ONYX.$OPT['compile']);
define('_TEMPLATE_CONFIG',ONYX.$OPT['config']);
define('_TEMPLATE_CACHE',ONYX.$OPT['cache']);

class Template extends Smarty
	{
		//var $compile_check = false;
		//var $force_compile = true;

		public function __construct()
		{
			parent::__construct();
			$this->setTemplateDir(_TEMPLATE_DIR);
			$this->setCompileDir(_TEMPLATE_COMPILE);
			$this->setCacheDir(_TEMPLATE_CACHE);
			//SetCONFIGDIR !
		}
	}

?>