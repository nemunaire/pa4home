<?php
if(!defined('ONYX')) exit;

$_ONYX_OPT_CAPTCHA = $OPT;

class Captcha
{
	static function check($entry)
	{
	  $SESS = new Session();

	  return isset($SESS->values["_captcha"]) && $SESS->values["_captcha"] == strtolower($entry);
	}

	private static function genImg()
	{
		global $_ONYX_OPT_CAPTCHA;
		$SESS = new Session();

		//Création de l'image à partir d'un fond
		$_img = imagecreate($_ONYX_OPT_CAPTCHA["sizeX"], $_ONYX_OPT_CAPTCHA["sizeY"]);
		imagefilledrectangle($_img, 0, 0, $_ONYX_OPT_CAPTCHA["sizeX"], $_ONYX_OPT_CAPTCHA["sizeY"], imagecolorallocate($_img, $_ONYX_OPT_CAPTCHA["back"]["red"], $_ONYX_OPT_CAPTCHA["back"]["green"], $_ONYX_OPT_CAPTCHA["back"]["blue"]));

		//Définition de la liste des caractères
		if (empty($_ONYX_OPT_CAPTCHA["caracteres"]))
			$caracteres = "2345678azertypsdfhjkmwxcbn";
		else
			$caracteres = $_ONYX_OPT_CAPTCHA["caracteres"];
		$nb_caracteres = strlen($caracteres) - 1;

		//On récupère tous les fichiers de polices
		if (empty($_ONYX_OPT_CAPTCHA["fonts_dir"]))
			$fontsDir = ONYX."modules/captcha/fonts";
		else
			$fontsDir = ONYX.$_ONYX_OPT_CAPTCHA["fonts_dir"];

		$fonts = glob($fontsDir."/*.ttf");
		$nb_fonts = count($fonts) - 1;

		//On définit des couleurs
		if (empty($_ONYX_OPT_CAPTCHA["colors"]) || !is_array($_ONYX_OPT_CAPTCHA["colors"]))
			$colors = array(
				imagecolorallocate($_img, rand(250,225), rand(0,25), rand(0,25)),
				imagecolorallocate($_img, rand(0,25), rand(250,225), rand(0,25)),
				imagecolorallocate($_img, rand(0,55), rand(0,55), rand(250,255)),
				imagecolorallocate($_img, rand(0,25), rand(250,225), rand(250,225)),
				imagecolorallocate($_img, rand(250,225), rand(0,25), rand(250,225)),
				imagecolorallocate($_img, rand(250,225), rand(250,225), rand(0,25)),
				//imagecolorallocate($_img, rand(200,225), rand(200,225), rand(200,225))
				imagecolorallocate($_img, rand(0,55), rand(0,55), rand(0,55))
			);
		else
			$colors = $_ONYX_OPT_CAPTCHA["colors"];
		$nb_colors = count($colors) - 1;

		//On définit la couleur de fond
		$arriere_plan = imagecolorallocatealpha($_img, 0, mt_rand(0,255), 0, 127);

		$SESS->values["_captcha"] = "";

		$dist = $_ONYX_OPT_CAPTCHA["sizeX"] / $_ONYX_OPT_CAPTCHA["nb_carac"];
		$dist2 = $dist / 2;
		$dist4 = $dist2 / 2;
		$height2 = $_ONYX_OPT_CAPTCHA["sizeY"] / 2;
		$height4 = $height2 / 2;

		//Si la configuration indique un fichier contenant des mots, on l'utilise
		if (!empty($_ONYX_OPT_CAPTCHA["mots"]) && is_readable(ONYX."modules/captcha/".$_ONYX_OPT_CAPTCHA["mots"]))
		  {
		    $mot = file(ONYX."modules/captcha/".$_ONYX_OPT_CAPTCHA["mots"], FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
		    $mot = $mot[mt_rand(0, count($mot) - 1)];
		  }

		//Création du captcha en lui-même
		for($i = 0; $i < $_ONYX_OPT_CAPTCHA["nb_carac"]; $i++)
		{
		  if (empty($mot) || empty($mot[$i]))
		    {
		      if (mt_rand(0,2) == 1)
			$char = strtoupper($caracteres[mt_rand(0, $nb_caracteres)]);
		      else
			$char = $caracteres[mt_rand(0, $nb_caracteres)];
		    }
		  else
		    $char = $mot[$i];

		  ImageTTFText($_img, mt_rand(14,23), mt_rand(-17, 20), $i * $dist + mt_rand(0,$dist2), mt_rand(0, $height4) + $height2, $colors[mt_rand(0, $nb_colors)], $fonts[mt_rand(0, $nb_fonts)], $char);

		  $SESS->values["_captcha"] .= strtolower($char);
		}
		$SESS->put();

		return $_img;
	}

	static function newCaptcha()
	{
		ob_start();
		imagepng(Captcha::genImg());
		return base64_encode(ob_get_clean());
	}

	static function _debug()
	{
		return base64_decode(Captcha::newCaptcha());
	}

	static function printNewCaptcha()
	{
		print "data:image/png;base64,".Captcha::newCaptcha();
	}

	static function showNewCaptcha()
	{
		header ("Content-type: image/png");

		imagepng($_img = Captcha::genImg());
		imagedestroy($_img);
	}
}
?>