<?php

if(!defined('ONYX')) exit;

function unparse_config($create, $xml, $array, $node = 'var', $attribut = 'name')
{
  if (is_array($array))
    foreach($array as $name => $var)
      {
	if (is_array($var))
	  {
	    $xml_var = $create->createElement($node);
	    $xml_var->setAttribute($attribut, $name);
	    
	    unparse_config($create, $xml_var, $var, $node, $attribut);
	    
	    $xml->appendChild($xml_var);
	  }
	else
	  {
	    $xml_var = $create->createElement($node, $var);
	    $xml_var->setAttribute($attribut, $name);
	    $xml->appendChild($xml_var);
	  }
      }
}

require_once("__config.class.php");
require_once("chconfig.class.php");
require_once("chmodules.class.php");

function chconfig($type, $file = null)
{
  $sortie = "";

  if ($type == "modules")
    {
      $sortie .= '<h1>Modification des paramètres de modules</h1><form action="?t=modules" method="post">';

      //On charge ce qu'on veut afficher
      $mods = new chModules($file);

      $sortie .= '</form>';
    }
  elseif ($type == "bdd")
    {
      $sortie .= "<h1>Modification des paramètres de base de données</h1>";
    }
  else
    {
      $sortie .= "<h1>Modification des paramètres de configuration</h1>";
    }

  return $sortie;
}

//Affichage automatique
if (!empty($OPT["show"]))
  {
    echo "<html><head><title>Modification des paramètres d'Onyx2</title></head><body>";
    echo chconfig(gpc("t"));
    echo "</body></html>";
    exit;
  }
?>