<?php

if(!defined('ONYX')) exit;

class chModules
{
  //Adresse vers le fichier de configuration en cours de modification
  private $_filename;

  //Liste des modules
  var $modules = array();


  // Constructeur qui charge le fichier de modules
  function __construct($filename = null)
  {
    if (!empty($filename))
      $this->_filename = $filename;
    else
      $this->_filename = ONYX."modules/modules.xml";

    //On charge le fichier
    $xml = new DOMDocument();
    $xml->load($this->_filename);
    
    foreach($xml->getElementsByTagName('module') as $value)
      {
	$this->modules[$value->getAttribute('name')] = parse_config($value->getElementsByTagName("default")->item(0), "option", "name");
      }
  }

  function write()
  {
    $xml = new DOMDocument('1.0', 'UTF-8');
    $xml->formatOutput = true;

    $xml_modules = $xml->createElement("modules");

    foreach($this->modules as $name => $module)
      {
	$xml_module = $xml->createElement("module");
	$xml_module->setAttribute("name", $name);

	$xml_def = $xml->createElement("default");
	unparse_config($xml, $xml_def, $module, "option", "name");

	$xml_module->appendChild($xml_def);
	$xml_modules->appendChild($xml_module);
      }

    $xml->appendChild($xml_modules);

    return $xml;
  }
}

?>