<?php

if(!defined('ONYX')) exit;

class __config
{
  //Tableau des variables de la configuration
  var $vars = array();

  //Tableau des variables d'environnement
  var $envs = array();

  //Tableau des modules
  var $modules = array();

  function __construct($xml = null)
  {
    if (isset($xml))
      {
	$this->vars = parse_config($xml, "var", "name");
	$this->envs = parse_config($xml, "env", "option");

	foreach($xml->getElementsByTagName('module') as $module)
	  $this->modules[$module->getAttribute('name')] = parse_config($module, "option", "name");
      }
  }
}
?>