<?php

if(!defined('ONYX')) exit;

define('PHPMAILER_DIR', ONYX.'modules/mail/');

require_once(PHPMAILER_DIR."class.phpmailer.php");

define('_MAIL_MAILER', $OPT['mailer']);
define('_MAIL_LANG', $OPT['lang']);
define('_MAIL_CHARSET', $OPT['charset']);
define('_MAIL_HOST', $OPT['host']);
define('_MAIL_PORT', $OPT['port']);
define('_MAIL_SECURE', $OPT['secure']);
define('_MAIL_FROM', $OPT['from']);
define('_MAIL_FROMNAME', $OPT['fromname']);
define('_MAIL_AUTH', !empty($OPT['username']));
define('_MAIL_USERNAME', $OPT['username']);
define('_MAIL_PASSWORD', $OPT['password']);

class Mailer extends PHPmailer
{ 
  var $CharSet = _MAIL_CHARSET;
  var $From = _MAIL_FROM;
  var $FromName = _MAIL_FROMNAME;
  var $Mailer = _MAIL_MAILER;
  var $Version = "2.0";
  var $Host = _MAIL_HOST;
  var $Port = _MAIL_PORT;
  var $SMTPSecure = _MAIL_SECURE;
  var $SMTPAuth = _MAIL_AUTH;
  var $Username = _MAIL_USERNAME;
  var $Password = _MAIL_PASSWORD;
  var $Timeout = 5;

  var $Sendmail = "/usr/lib/sendmail";
}
?>