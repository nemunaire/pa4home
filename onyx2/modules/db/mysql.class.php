<?php

class BDD
    {
        var $connected;
        
        private $session;
        
        private $reponse;
        
        var $host;
        
        var $user;
        
        private $password;
        
        var $database;
        
        var $num_rows;
        
        var $nodb;
        
        
        function __construct($profile=NULL)
            {
                if($profile === FALSE) return FALSE;
                
                global $db_config;
                
                if(empty($profile))
                    {
                        if(!$db_config['profile']) return FALSE;
                        $profile = &$db_config['profile'];
                    }
                
                if(!ctype_alnum($profile)) trigger_error('Le nom du profil contient des caracteres illegaux',E_USER_ERROR);
                
                if($db_config['profile'])
                    {
                        require(ONYX.'db/'.$profile.'.profile.php');
                        
                        $db = &$___profile['db'];
                        $host = &$___profile['host'];
                        $user = &$___profile['user'];
                        $pass = &$___profile['pass'];
                    }
                
                if($db_config['crypt']) $pass = dbpass($pass,$db_config['crypt']);
                
                return $this->connexion($host,$user,$pass,$db);
            }
        
        function connexion($host,$user,$pass,$db=NULL)
            {
                if($this->session) $this->deconnexion();
                
                $this->reponse = NULL;
                
                $this->session = mysql_connect($host,$user,$pass);
                
                if(!$this->session)
                    {
                        elog('Connexion impossible a la base de donnee : '.$this->erreur(),2);
                        if(function_exists($this->nodb)) call_user_func($this->nodb);
                        return FALSE;
                    }
                
                mysql_query('SET CHARACTER SET "utf8"',$this->session);
                
                $this->host = $host;
                $this->user = $user;
                $this->password = $pass;
                if($db) $this->db($db);
                
                $this->connected = TRUE;
		return TRUE;
            }
        
        function reconnexion()
            {
                if(!empty($this->host) && !empty($this->user) && !empty($this->password) && !empty($this->database)) return $this->connexion($this->host,$this->user,$this->password,$this->database);
            }
        
        function deconnexion()
            {
                if(!$this->session) return FALSE;
                
                $r =  mysql_close($this->session);
                $this->session = FALSE;
                $this->connected = FALSE;
                return $r;
            }
        
        function erreur()
            {
                if(!$this->session) return FALSE;
                
                return mysql_error($this->session);
            }
        
        function db($db=NULL)
            {
                if(!$this->session) return FALSE;
                
                return $this->database = mysql_select_db($db,$this->session) ? $db : $this->database;
            }
        
        function escape(&$var)
            {
                if(!$this->session) return FALSE;
                $var = mysql_real_escape_string($var,$this->session);
                return $var;
            }
        
        function query($query)
            {
                if(!$this->session) return FALSE;
                
                $this->reponse = mysql_query($query,$this->session);
                
                global $db_config;
                
                if(!$this->reponse && $db_config['log']) elog('Erreur Mysql: " '.$this->erreur().' ", avec la requète: { '.$query.' }.',1);
                
                $this->num_rows = @mysql_num_rows($this->reponse);
                
                if($this->num_rows == 0) return NULL;
                
                elseif($this->num_rows >= 1)
                    {
                        for($i=0; $var = mysql_fetch_assoc($this->reponse); $i++) $sortie[$i] = $var;
                        return $sortie;
                    }
                
                else return FALSE;
            }
        
        function unique_query($query)
            {
                    if(!$this->session) return FALSE;
                    
                    $this->reponse = mysql_query($query,$this->session);
                    
                    global $db_config;
                    
                    if(!$this->reponse && $db_config['log']) elog('Erreur Mysql: " '.$this->erreur().' ", avec la requète: { '.$query.' }.',1);
                    
                    $this->num_rows = @mysql_num_rows($this->reponse);
                    
                    if($this->num_rows == 0) return NULL;
                    
                    elseif($this->num_rows >= 1) return mysql_fetch_assoc($this->reponse);
                    
                    else return FALSE;
            }
    	
    	function affected()
            {
                if(!$this->session) return FALSE;
                
                return mysql_affected_rows($this->session);
            }

	function insert_id()
	{
	  if(!$this->session) return FALSE;
                
	  return mysql_insert_id($this->session);
	}
    }
?>
