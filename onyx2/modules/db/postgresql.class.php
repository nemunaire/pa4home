<?php

class BDD
    {
        var $connected;
        
        private $session;
        
        private $reponse;
        
        var $host;
        
        var $user;
        
        private $password;
        
        var $database;
        
        var $num_rows;
        
        var $nodb;
        
        
        function __construct($profile=NULL)
            {
                if($profile === FALSE) return FALSE;
                
                global $db_config;
                
                if(empty($profile))
                    {
                        if(!$db_config['profile']) return FALSE;
                        $profile = &$db_config['profile'];
                    }
                
                if(!ctype_alnum($profile)) trigger_error('Le nom du profil contient des caracteres illegaux',E_USER_ERROR);
                
                if($db_config['profile'])
                    {
                        require(ONYX.'db/'.$profile.'.profile.php');
                        
                        $db = &$___profile['db'];
                        $host = &$___profile['host'];
                        $user = &$___profile['user'];
                        $pass = &$___profile['pass'];
                    }
                
                if($db_config['crypt']) $pass = dbpass($pass,$db_config['crypt']);
                
                return $this->connexion($host,$user,$pass,$db);
            }
        
        function connexion($host,$user,$pass,$db=NULL)
            {
                if($this->session) $this->deconnexion();
                
                $this->reponse = NULL;
                
                $host = pg_escape_string($host);
                $user = pg_escape_string($user);
                $pass = pg_escape_string($pass);
                $db = pg_escape_string($db);
                
                $this->session = pg_connect("host='$host' port=5432 dbname='$db' user='$user' password='$pass'");
                
                if(!$this->session)
                    {
                        elog('Connexion impossible a la base de donnee : '.$this->erreur(),2);
                        if(function_exists($this->nodb)) call_user_func($this->nodb);
                        return FALSE;
                    }
                
                pg_setclientencoding($this->session,'UTF8');
                
                $this->host = $host;
                $this->user = $user;
                $this->password = $pass;
                $this->database = $db;
                
                $this->connected = TRUE;
		return TRUE;
            }
        
                function reconnexion()
            {
                if(!empty($this->host) && !empty($this->user) && !empty($this->password) && !empty($this->database)) return $this->connexion($this->host,$this->user,$this->password,$this->database);
            }
        
        function deconnexion()
            {
                if(!$this->session) return FALSE;
                
                $r =  pg_close($this->session);
                $this->session = FALSE;
                $this->connected = FALSE;
                return $r;
            }
        
        function erreur()
            {
                if(!$this->session) return FALSE;
                
                return pg_last_error($this->session);
            }
        
        function db($db)
            {
                if(!$this->session) return FALSE;
                
                return $this->database = pg_query($this->session,"\\connect ".pg_escape_string($db)) ? $db : $this->database;
            }
        
        function escape(&$var)
            {
                if(!$this->session) return FALSE;
                $var = pg_escape_string($this->session,$var);
                return $var;
            }
        
        function query($query)
            {
                if(!$this->session) return FALSE;
                
                $this->reponse = pg_query($this->session,$query);
                
                global $db_config;
                
                if(!$this->reponse && $db_config['log']) elog('Erreur PostgreSQL: " '.$this->erreur().' ", avec la requète: { '.$query.' }.',1);
                
                $this->num_rows = pg_num_rows($this->reponse);
                
                if($this->num_rows == 0) return NULL;
                
                elseif($this->num_rows >= 1)
                    {
                        for($i=0; $var = pg_fetch_assoc($this->reponse); $i++) $sortie[$i] = $var;
                        return $sortie;
                    }
                
                else return FALSE;
            }
        
        function unique_query($query)
            {
                    if(!$this->session) return FALSE;
                    
                    $this->reponse = pg_query($this->session,$query);
                    
                    global $db_config;
                    
                    if(!$this->reponse && $db_config['log']) elog('Erreur PostgreSQL: " '.$this->erreur().' ", avec la requète: { '.$query.' }.',1);
                    
                    $this->num_rows = pg_num_rows($this->reponse);
                    
                    if($this->num_rows == 0) return NULL;
                    
                    elseif($this->num_rows >= 1) return pg_fetch_assoc($this->reponse);
                    
                    else return FALSE;
            }
    	
    	function affected()
            {
                if(!$this->session) return FALSE;
                
                return pg_affected_rows($this->reponse);
            }
    }
?>
