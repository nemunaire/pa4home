<?php

class Cache
    {
    
        static function set($id,$var)
            {
                $file = '<?php $cache = '.var_export($var,TRUE).'; ?>';
                
                file_put_contents(ONYX.'cache/'.md5($id).'.cache.php',$file) or trigger_error('dossier cache inaccessible en écriture.',E_USER_ERROR);
            }
        
        static function read($id)
            {
                if(!is_readable(ONYX.'cache/'.md5($id).'.cache.php')) return FALSE;
                
                include(ONYX.'cache/'.md5($id).'.cache.php');
                if(!$cache) return FALSE;
                return $cache;
            }
        
        static function del($id)
            {
                if(!is_file(ONYX.'cache/'.md5($id).'.cache.php')) return FALSE;
                return unlink(ONYX.'cache/'.md5($id).'.cache.php');
            }
        
        static function flush()
            {
                foreach(glob(ONYX.'cache/*.cache.php') as $file) unlink($file);
            }
    
    }

?>