<?php

function elog($message,$level = 0,$dir = NULL,$file = NULL)
	{
		$dir = empty($dir) ? ONYX."log" : $dir;
		$file = empty($file) ? strftime('%d-%m-%y').".log" : $file;
		
		if($fichier = fopen("$dir/$file",'a+'))
			{
				switch($level)
					{
						default:
						case 0: $level = 'MESSAGE'; break;
						case 1: $level = 'AVERTISSEMENT'; break;
						case 2: $level = 'ERREUR'; break;
					}
				$time = strftime('%d/%m/%y %H:%M:%S');
				$req = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD']: '';
				$arg = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']: '';
				$remote = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '';
				$ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT']: '';
				$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']: '';
				$line = "[$time] $level : $message  [$remote] [$req] [$arg] [$ua] [$referer]\r\n";
				fwrite($fichier,$line);
				fclose($fichier);
			}
		else trigger_error('Log non accessible en ecriture',E_USER_ERROR);
	}

function gpc($name,$method='get')
	{
		switch($method)
			{
				default:
				case 'get': $var = isset($_GET[$name]) ? $_GET[$name] : NULL; break;
				case 'post': $var = isset($_POST[$name]) ? $_POST[$name] : NULL; break;
				case 'cookie': $var = isset($_COOKIE[$name]) ? $_COOKIE[$name] : NULL; break;
			}
		
		if(get_magic_quotes_gpc()) $var = stripslashes($var);
		return $var;
	}

/*function cookie($name,$value,$time = 3600)
	{
		setcookie($name,$value,$time);
	}*/

?>